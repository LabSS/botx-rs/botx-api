use botx_api::api::{context::BotXApiContext, v2::token::api::token};
use env_logger::Env;


#[tokio::main]
async fn main() -> anyhow::Result<()> {
    dotenv::dotenv().ok();
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let mut context = BotXApiContext::from_env_with_default_client();

    let response = token(&mut context).await.unwrap();

    tracing::info!("Auth token:[{}]", response.token);

    Ok(())
}