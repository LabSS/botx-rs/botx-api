use crate::api::context::BotXApiContext;
use anthill_di::{types::AddDependencyResult, DependencyContext, LifeCycle, ServiceMappingBuilder};
use async_lock::RwLock;
use easy_ext::ext;

#[ext(IBotXApiContextExt)]
#[async_trait_with_sync::async_trait]
pub impl DependencyContext {
    async fn register_botx_api_context(
        &self,
    ) -> AddDependencyResult<ServiceMappingBuilder<RwLock<BotXApiContext>>> {
        self.register_type::<BotXApiContext>(LifeCycle::Transient)
            .await?;
        let ctx = self.resolve::<BotXApiContext>().await.unwrap();
        self.register_instance(RwLock::new(ctx)).await
    }
}
