#[macro_use]
extern crate derive_builder;

pub mod bot;
pub mod api;
pub mod extensions;