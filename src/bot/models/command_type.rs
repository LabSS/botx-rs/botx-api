use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum CommandType {
    #[serde(rename(serialize = "user", deserialize = "user"))]
    User,
    #[serde(rename(serialize = "system", deserialize = "system"))]
    System,
}