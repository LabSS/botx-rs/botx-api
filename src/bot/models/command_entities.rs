use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::api::models::*;

use super::Attachment;

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
#[serde(tag = "type", content = "data")]
pub enum CommandEntities {
    #[serde(rename(serialize = "mention", deserialize = "mention"))]
    Mention(Mention),

    #[serde(rename(serialize = "forward", deserialize = "forward"))]
    Forward(Forward),

    #[serde(rename(serialize = "reply", deserialize = "reply"))]
    Reply(Reply),
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct Forward {
    /// id чата откуда переслали сообщение
    pub group_chat_id: Uuid,

    /// huid автора сообщения
    pub sender_huid: Uuid,

    /// chat|channel
    pub forward_type: ForwardType,

    /// (Optional) - имя чата откуда переслали сообщение
    pub source_chat_name: Option<String>,

    /// (Optional) - sync_id пересылаемого сообщения
    pub source_sync_id: Option<Uuid>,

    /// ts пересылаемого сообщения
    pub source_inserted_at: DateTime<Utc>,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum ForwardType {
    #[serde(rename(serialize = "chat", deserialize = "chat"))]
    Chat,
    #[serde(rename(serialize = "channel", deserialize = "channel"))]
    Channel,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct Reply {
    /// sync_id исходного сообщения
    pub source_sync_id: Uuid,

    /// huid автора сообщения
    pub sender: Uuid,

    /// текст исходного сообщения
    pub body: String,

    /// меншены исходного сообщения
    pub mentions: Vec<Mention>,

    /// вложение исходного сообщения
    pub attachment: Attachment,

    /// chat|botx|group_chat|channel
    pub reply_type: ReplyType,

    /// (Optional) - ID чата откуда переслали сообщение
    pub source_group_chat_id: Option<Uuid>,

    /// (Optional) - имя чата откуда переслали сообщение
    pub source_chat_name: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum ReplyType {
    #[serde(rename(serialize = "chat", deserialize = "chat"))]
    Chat,
    #[serde(rename(serialize = "botx", deserialize = "botx"))]
    Botx,
    #[serde(rename(serialize = "group_chat", deserialize = "group_chat"))]
    GroupChat,
    #[serde(rename(serialize = "channel", deserialize = "channel"))]
    Channel,
}
