use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::api::models::Status;

use super::ChatType;

#[derive(Deserialize, Debug)]
pub struct StatusRequest {
    pub bot_id: Uuid,
    pub user_huid: Uuid,
    pub ad_login: Option<String>,
    pub ad_domain: Option<String>,
    pub is_admin: Option<bool>,
    pub chat_type: Option<ChatType>,
}

#[derive(Serialize, Debug, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct StatusResponse {
    /// Успешность обработки ответа “ok”|“error”
    pub status: Status,

    /// Ответ
    pub result: StatusResponseResult,
}

#[derive(Serialize, Debug, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct StatusResponseResult {
    /// Бот включен/выключен
    pub enabled: bool,

    /// Текущий статус бота
    #[builder(default)]
    pub status_message: Option<String>,

    /// Список команд бота 
    #[builder(default)]
    pub commands: Vec<BotCommand>,
}

/// Команда бота 
#[derive(Serialize, Debug, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct BotCommand {
    /// Описание команды
    pub description: String,

    /// Тело команды
    pub body: String,

    /// Имя команды
    pub name: String,
}