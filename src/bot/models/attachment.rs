// #[derive(Debug, Serialize, Deserialize)]
// pub struct Attachment {
//     /// тип вложения
//     #[serde(rename(serialize = "type", deserialize = "type"))] // лексема type зарезервирована
//     pub attachment_type: AttachmentType,

//     /// объект с данными вложения
//     pub data: AttachmentData,
// }

/// тип вложения
// #[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
// pub enum AttachmentType {
//     #[serde(rename(serialize = "image", deserialize = "image"))]
//     Image,
//     #[serde(rename(serialize = "video", deserialize = "video"))]
//     Video,
//     #[serde(rename(serialize = "document", deserialize = "document"))]
//     Document,
//     #[serde(rename(serialize = "voice", deserialize = "voice"))]
//     Voice,
//     #[serde(rename(serialize = "link", deserialize = "link"))]
//     Link,
// }


use serde::{Serialize, Deserialize};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
#[serde(tag = "type", content = "data")]
//#[serde(untagged)]
pub enum Attachment/* Data */ {
    #[serde(rename(serialize = "image", deserialize = "image"))]
    Image(ImageAttachment),
    #[serde(rename(serialize = "video", deserialize = "video"))]
    Video(VideoAttachment),
    #[serde(rename(serialize = "document", deserialize = "document"))]
    Document(DocumentAttachment),
    #[serde(rename(serialize = "voice", deserialize = "voice"))]
    Voice(VoiceAttachment),
    #[serde(rename(serialize = "location", deserialize = "location"))]
    Location(LocationAttachment),
    #[serde(rename(serialize = "contact", deserialize = "contact"))]
    Contact(ContactAttachment),
    #[serde(rename(serialize = "link", deserialize = "link"))]
    Link(LinkAttachment),
    #[serde(rename(serialize = "sticker", deserialize = "sticker"))]
    Sticker(StickerAttachment),
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct ImageAttachment {
    /// содержимое файла в формате data URL + base64 data (RFC 2397)
    pub content: String,

    /// имя файла
    pub file_name: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct VideoAttachment {
    /// содержимое файла в формате data URL + base64 data (RFC 2397)
    pub content: String,

    /// имя файла
    pub file_name: Option<String>,

    /// длительность воспроизведения
    pub duration: u32,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct DocumentAttachment {
    /// содержимое файла в формате data URL + base64 data (RFC 2397)
    pub content: String,

    /// имя файла
    pub file_name: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct VoiceAttachment {
    /// содержимое файла в формате data URL + base64 data (RFC 2397)
    pub content: String,

    /// длительность воспроизведения
    pub duration: u32,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct LocationAttachment {
    /// название местоположения
    pub location_name: String,

    /// адрес
    pub location_address: String,

    /// широта
    pub location_lat: f32,

    /// долгота
    pub location_lng: f32,
}

impl Eq for LocationAttachment { }

impl PartialEq for LocationAttachment {
    fn eq(&self, other: &Self) -> bool {
        self.location_name == other.location_name && self.location_address == other.location_address && self.location_lat == other.location_lat && self.location_lng == other.location_lng
    }
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct ContactAttachment {
    /// имя файла
    pub file_name: String,

    /// имя контакта
    pub contact_name: String,

    /// содержимое файла в формате data URL + base64 data (RFC 2397)
    pub content: String,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct LinkAttachment {
    /// урл ссылки
    pub url: String,

    /// заголовок
    pub url_title: Option<String>,

    /// урл изображения для предварительного просмотра ссылки
    pub url_preview: Option<String>,

    /// текст ссылки
    pub url_text: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct StickerAttachment {
    /// id стикера
    pub id: Uuid,

    /// ссылка на стикер
    pub link: String,

    /// id стикерпака
    pub pack: Uuid,

    /// версия файла
    pub version: u32,
}