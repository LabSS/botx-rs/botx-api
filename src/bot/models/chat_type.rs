use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone, Default)]
pub enum ChatType {
    #[default]
    #[serde(rename(serialize = "chat", deserialize = "chat"))]
    Chat,
    #[serde(rename(serialize = "group_chat", deserialize = "group_chat"))]
    GroupChat,
    #[serde(rename(serialize = "channel", deserialize = "channel"))]
    Channel,
}