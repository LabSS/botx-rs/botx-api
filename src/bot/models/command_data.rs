use serde::{Serialize, Deserialize};
use uuid::Uuid;

use super::chat_type::ChatType;

// #[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
// #[serde(untagged)]
// pub enum CommandData {
//     /// Событие отправляется при создание чата
//     ChatCreated(ChatCreatedCommand),
//     /// Событие отправляется при добавление мемберов в чат
//     AddedToChat(AddedToChatCommand),
//     /// Событие отправляется при удалении администратором участников чата
//     DeletedFromChat(DeletedFromChatCommand),
//     /// Событие отправляется при выходе участников из чата
//     LeftFromChat(LeftFromChatCommand),
//     /// Событие отправляется клиентом при взаимодействии со smartapp приложением
//     SmartappEvent(SmartappEventCommand),
//     /// Событие отправляется ботом при взаимодействие с другими ботами
//     InternalBotNotification(InternalBotNotificationCommand),
//     /// Событие отправляется при успешном логине пользователя на CTS
//     CtsLogin(CtsLoginLogoutCommand),
//     /// Событие отправляется при успешном выходе пользователя с CTS
//     CtsLogout(CtsLoginLogoutCommand),
//     /// Текстовая информация события
//     Text(String),
//     /// Кстомная информация события
//     Other(serde_json::Value),
// }

// impl CommandData {
//     pub fn unwrap_other(&self) -> &serde_json::Value {
//         match self {
//             CommandData::Other(other) => other,
//             _ => panic!("Unexpected command data"),
//         }
//     }
// }

/// Событие отправляется при создание чата
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct ChatCreatedCommandData {
    /// uuid чата
    pub group_chat_id: Uuid,

    /// тип чата
    pub chat_type: ChatType,

    /// имя чата
    pub name: String,

    /// huid создателя чата
    pub creator: Uuid,
    
    pub members: Vec<ChatCreatedCommandMembers>,
}

/// Событие отправляется при удалении чата пользователем
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct ChatDeletedByUserCommandData {
    /// uuid чата
    pub group_chat_id: Uuid,

    /// huid пользователя
    pub user_huid: Uuid,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct ChatCreatedCommandMembers {
    /// huid участника  
    pub huid: Uuid,

    /// имя участника
    pub name: Option<String>,

    /// тип участника
    pub user_kind: UserKind,

    /// является/не является админом
    pub admin: bool,
}

/// тип участника
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone, Copy)]
pub enum UserKind {
    #[serde(rename(serialize = "user", deserialize = "user"))]
    User,
    #[serde(rename(serialize = "cts_user", deserialize = "cts_user"))]
    CtsUser,
    #[serde(rename(serialize = "unregistered", deserialize = "unregistered"))]
    Unregistered,
    #[serde(rename(serialize = "botx", deserialize = "botx"))]
    Botx
}

/// Событие отправляется при добавление мемберов в чат
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct AddedToChatCommandData {
    /// список добавленных мемберов
    pub added_members: Vec<Uuid>,
}

/// Событие отправляется при удалении администратором участников чата
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct DeletedFromChatCommandData {
    /// список идентификаторов удаленных пользователей
    pub deleted_members: Vec<Uuid>,
}

/// Событие отправляется при выходе участников из чата
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct LeftFromChatCommandData {
    /// список идентификаторов покинувших чат пользователей
    pub left_members: Vec<Uuid>,
}

/// Событие отправляется при выходе участников из чата
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct EventEditCommandData {
    /// список идентификаторов покинувших чат пользователей
    pub body: String,
}

/// Событие отправляется клиентом при взаимодействии со smartapp приложением
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct SmartappEventCommandData {
    /// уникальный идентификатор запроса
    #[serde(rename(serialize = "ref", deserialize = "ref"))] // ref зарезервировано
    pub request_ref: Uuid,

    /// ID смарт аппа
    pub smartapp_id: Uuid,

    /// пользовательские данные
    pub data: serde_json::Value,

    /// опции запроса
    pub opts: serde_json::Value,

    /// версия протокола smartapp <-> bot
    pub smartapp_api_version: u16,
}

/// Событие отправляется ботом при взаимодействие с другими ботами
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct InternalBotNotificationCommandData {
    /// пользовательские данные
    pub data: serde_json::Value,

    /// опции запроса
    pub opts: serde_json::Value,
}

/// Событие отправляется при успешном логине пользователя на CTS
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct CtsLoginCommandData {
    /// huid пользователя
    pub user_huid: Uuid,

    /// id сервера на который был произведен логин или <br/>
    /// id сервера с которого был произведен выход
    pub cts_id: Uuid,
}

/// Событие отправляется при успешном выходе пользователя с CTS
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct CtsLogoutCommandData {
    /// huid пользователя
    pub user_huid: Uuid,

    /// id сервера на который был произведен логин или <br/>
    /// id сервера с которого был произведен выход
    pub cts_id: Uuid,
}