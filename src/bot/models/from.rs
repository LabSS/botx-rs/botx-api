use serde::{Serialize, Deserialize};
use uuid::Uuid;

use super::chat_type::ChatType;

#[derive(Debug, Serialize, Deserialize, Eq, PartialEq)]
pub struct From {
    /// (Default: null) - huid юзера который отправил команду
    pub user_huid: Option<Uuid>,

    /// (Default: null) - id чата в который отправили команда
    pub group_chat_id: Option<Uuid>,
    /// (Default: null) - тип чата (chat|group_chat|channel)
    pub chat_type: Option<ChatType>,

    /// (Default: null) - логин юзера который отправил команду
    pub ad_login: Option<String>,

    /// (Default: null) - домен юзера который отправил команду
    pub ad_domain: Option<String>,

    /// (Default: null) - имя юзера который отправил команду
    pub username: Option<String>,

    /// (Default: null) - является ли юзер админом чата
    pub is_admin: Option<bool>,

    /// (Default: null) - является ли юзер создателем чата
    pub is_creator: Option<bool>,

    /// (Default: null) - имя бренда производителя
    pub manufacturer: Option<String>,

    /// (Default: null) - название девайса
    pub device: Option<String>,

    /// (Default: null) - ОС девайса
    pub device_software: Option<String>,

    /// (Default: null)
    pub device_meta: Option<DeviceMeta>,

    ///  (Default: null) - название клиентской платформы (web|android|ios|desktop)
    pub platform: Option<Platform>,


    /// (Default: null) - идентификатор пакета с данными приложения и устройства
    pub platform_package_id: Option<String>,

    /// (Default: null) - версия приложения Express
    pub app_version: Option<String>,

    ///  (Default: null) - локаль текущей сессии
    pub locale: Option<String>,

    /// имя хоста с которого пришла команда
    pub host: Option<String>,
}

#[derive(Debug, Default, Serialize, Deserialize, Eq, PartialEq)]
pub struct DeviceMeta {
    ///  разрешение приложению на отправку пушей
    pub pushes: Option<bool>,
    
    /// таймзона пользователя
    pub timezone: Option<String>,
    
    /// различные разрешения приложения (использование микрофона, камеры и тд)
    pub permissions: Option<Permissions>,
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub enum Platform {
    #[serde(rename(serialize = "web", deserialize = "web"))]
    Web,
    #[serde(rename(serialize = "android", deserialize = "android"))]
    Android,
    #[serde(rename(serialize = "ios", deserialize = "ios"))]
    Ios,
    #[serde(rename(serialize = "desktop", deserialize = "desktop"))]
    Desktop
}

#[derive(Debug, Default, Serialize, Deserialize, Eq, PartialEq)]
pub struct Permissions {
    pub contacts: Option<bool>,
    pub microphone: Option<bool>,
    pub notifications: Option<bool>,
    pub storage: Option<bool>,
}