use serde::{Serialize, Deserialize};

// #[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
// #[serde(untagged)]
// pub enum CommandBody {
//     /// Событие отправляется при создание чата
//     #[serde(rename(serialize = "system:chat_created", deserialize = "system:chat_created"))]
//     ChatCreated,

//     /// Событие отправляется при добавление мемберов в чат
//     #[serde(rename(serialize = "system:added_to_chat", deserialize = "system:added_to_chat"))]
//     AddedToChat,

//     /// Событие отправляется при удалении администратором участников чата
//     #[serde(rename(serialize = "system:deleted_from_chat", deserialize = "system:deleted_from_chat"))]
//     DeletedFromChat,

//     /// Событие отправляется при выходе участников из чата
//     #[serde(rename(serialize = "system:left_from_chat", deserialize = "system:left_from_chat"))]
//     LeftFromChat,

//     /// Событие отправляется клиентом при взаимодействии со smartapp приложением
//     #[serde(rename(serialize = "system:smartapp_event", deserialize = "system:smartapp_event"))]
//     SmartappEvent,

//     /// Событие отправляется ботом при взаимодействие с другими ботами
//     #[serde(rename(serialize = "system:internal_bot_notification", deserialize = "system:internal_bot_notification"))]
//     InternalBotNotification,

//     /// Событие отправляется при успешном логине пользователя на CTS
//     #[serde(rename(serialize = "system:cts_login", deserialize = "system:cts_login"))]
//     CtsLogin,

//     /// Событие отправляется при успешном выходе пользователя с CTS
//     #[serde(rename(serialize = "system:cts_logout", deserialize = "system:cts_logout"))]
//     CtsLogout,

//     Text(String),
//     Other(serde_json::Value),
// }

// impl CommandBody {
//     pub fn unwrap_text(&self) -> &String {
//         match self {
//             CommandBody::Text(data) => data,
//             _ => panic!("Unexpected command body")
//         }
//     }

//     pub fn unwrap_other(&self) -> &serde_json::Value {
//         match self {
//             CommandBody::Other(data) => data,
//             _ => panic!("Unexpected command body")
//         }
//     }

//     pub fn is_added_to_chat(&self) -> bool {
//         matches!(self, CommandBody::AddedToChat)
//     }
    
//     pub fn is_chat_created(&self) -> bool {
//         matches!(self, CommandBody::ChatCreated)
//     }

//     pub fn is_deleted_from_chat(&self) -> bool {
//         matches!(self, CommandBody::DeletedFromChat)
//     }

//     pub fn is_left_from_chat(&self) -> bool {
//         matches!(self, CommandBody::LeftFromChat)
//     }

//     pub fn is_smartapp_event(&self) -> bool {
//         matches!(self, CommandBody::SmartappEvent)
//     }

//     pub fn is_internal_bot_notification(&self) -> bool {
//         matches!(self, CommandBody::InternalBotNotification)
//     }

//     pub fn is_cts_login(&self) -> bool {
//         matches!(self, CommandBody::CtsLogin)
//     }

//     pub fn is_cts_logout(&self) -> bool {
//         matches!(self, CommandBody::CtsLogout)
//     }

//     pub fn is_text(&self) -> bool {
//         match self {
//             CommandBody::Text(_) => true,
//             _ => false
//         }
//     }

//     pub fn is_other(&self) -> bool {
//         match self {
//             CommandBody::Other(_) => true,
//             _ => false
//         }
//     }
// }

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
#[serde(untagged)]
pub enum CommandBody<TBody> {
    /// Событие отправляется при создание чата
    #[serde(rename(serialize = "system:chat_created", deserialize = "system:chat_created"))]
    ChatCreated,

    /// Событие отправляется при добавление мемберов в чат
    #[serde(rename(serialize = "system:added_to_chat", deserialize = "system:added_to_chat"))]
    AddedToChat,

    /// Событие отправляется при удалении администратором участников чата
    #[serde(rename(serialize = "system:deleted_from_chat", deserialize = "system:deleted_from_chat"))]
    DeletedFromChat,

    /// Событие отправляется при выходе участников из чата
    #[serde(rename(serialize = "system:left_from_chat", deserialize = "system:left_from_chat"))]
    LeftFromChat,

    /// Событие отправляется клиентом при взаимодействии со smartapp приложением
    #[serde(rename(serialize = "system:smartapp_event", deserialize = "system:smartapp_event"))]
    SmartappEvent,

    /// Событие отправляется ботом при взаимодействие с другими ботами
    #[serde(rename(serialize = "system:internal_bot_notification", deserialize = "system:internal_bot_notification"))]
    InternalBotNotification,

    /// Событие отправляется при успешном логине пользователя на CTS
    #[serde(rename(serialize = "system:cts_login", deserialize = "system:cts_login"))]
    CtsLogin,

    /// Событие отправляется при успешном выходе пользователя с CTS
    #[serde(rename(serialize = "system:cts_logout", deserialize = "system:cts_logout"))]
    CtsLogout,

    Body(TBody),
}

impl<TBody> CommandBody<TBody> {
    pub fn unwrap_body(&self) -> &TBody {
        match self {
            CommandBody::Body(data) => data,
            _ => panic!("Unexpected command body")
        }
    }

    pub fn is_added_to_chat(&self) -> bool {
        matches!(self, CommandBody::AddedToChat)
    }
    
    pub fn is_chat_created(&self) -> bool {
        matches!(self, CommandBody::ChatCreated)
    }

    pub fn is_deleted_from_chat(&self) -> bool {
        matches!(self, CommandBody::DeletedFromChat)
    }

    pub fn is_left_from_chat(&self) -> bool {
        matches!(self, CommandBody::LeftFromChat)
    }

    pub fn is_smartapp_event(&self) -> bool {
        matches!(self, CommandBody::SmartappEvent)
    }

    pub fn is_internal_bot_notification(&self) -> bool {
        matches!(self, CommandBody::InternalBotNotification)
    }

    pub fn is_cts_login(&self) -> bool {
        matches!(self, CommandBody::CtsLogin)
    }

    pub fn is_cts_logout(&self) -> bool {
        matches!(self, CommandBody::CtsLogout)
    }

    // pub fn is_text(&self) -> bool {
    //     match self {
    //         CommandBody::Text(_) => true,
    //         _ => false
    //     }
    // }

    pub fn is_body(&self) -> bool {
        match self {
            CommandBody::Body(_) => true,
            _ => false
        }
    }
}