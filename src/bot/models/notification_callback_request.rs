use std::collections::HashMap;

use serde::Deserialize;
use uuid::Uuid;

#[derive(Deserialize, Debug)]
pub struct NotificationCallbackRequestOk {
    /// sync id отправляемого сообщения
    pub sync_id: Uuid,
    /// (Default: {}) - результат успешного запроса
    pub result: HashMap<String, serde_json::Value>
}

#[derive(Deserialize, Debug)]
pub struct NotificationCallbackRequestError {
    /// sync id отправляемого сообщения
    pub sync_id: Uuid,
    /// краткая/общая причина ошибки
    pub reason: String,
    /// (Default: []) - детальное описание ошибки/ошибок
    pub errors: Vec<String>,
    /// (Default: {}) - метаданные ошибки
    pub error_data: HashMap<String, serde_json::Value> 
}