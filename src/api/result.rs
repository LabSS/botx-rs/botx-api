use serde::{Serialize, Deserialize};

pub type BotXApiResult<TResponse, TExpressErr> = Result<TResponse, BotXApiError<TExpressErr>>;

#[derive(Debug, thiserror::Error)]
pub enum BotXApiError<TExpressErr> {
    #[error("Express response error {0:?}")]
    ExpressError(TExpressErr),
    
    #[error("Reqwest error {0:?}")]
    ReqwestError(reqwest::Error),

    #[error("Serde error response error {0:?}")]
    SerdeError(serde_json::error::Error),

    #[error("Unauthorized request")]
    Unauthorized,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(tag = "status")]
pub enum ExpressResult<TResponse, TExpressErr> {
    #[serde(rename(serialize = "ok", deserialize = "ok"))]
    Ok(TResponse),
    #[serde(rename(serialize = "error", deserialize = "error"))]
    Err(TExpressErr),
}
 
impl<TResponse, TExpressErr> From<ExpressResult<TResponse, TExpressErr>> for BotXApiResult<TResponse, TExpressErr> {
    fn from(value: ExpressResult<TResponse, TExpressErr>) -> Self {
        match value {
            ExpressResult::Ok(res) => Ok(res),
            ExpressResult::Err(err) => Err(BotXApiError::ExpressError(err)),
        }
    }
}

impl<TResponse, TExpressErr> ExpressResult<TResponse, TExpressErr> {
    pub fn is_ok(&self) -> bool {
        // TODO: проверить замену на это: matches!(self, Self::Ok)
        match self {
            Self::Ok(_) => true,
            Self::Err(_) => false,
        }
    }
}

impl<TExpressErr> From<reqwest::Error> for BotXApiError<TExpressErr> {
    fn from(err: reqwest::Error) -> Self {
        BotXApiError::ReqwestError(err)
    }
}

impl<TExpressErr> From<serde_json::error::Error> for BotXApiError<TExpressErr> {
    fn from(err: serde_json::error::Error) -> Self {
        BotXApiError::SerdeError(err)
    }
}

impl<TExpressErr> BotXApiError<TExpressErr> {
    pub fn is_unauthorized(&self) -> bool {
        match self {
            Self::Unauthorized => true,
            _ => false,
        }
    }
}