use std::collections::HashMap;

use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::api::models::*;

/// Новое состояние редактируемого сообщения
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct EditMessageRequest {
    /// uuid редактируемого события
    pub sync_id: Uuid,

    /// Описание события
    pub payload: EventPayload,

    /// (Default: Skip) - файл в base64 представление. Если передать null, то существующий файл удалится из события 
    // #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub file: Option<File>,

    /// опции запроса
    #[builder(default)]
    pub opts: EditMessageRequestOptions,
}

/// Опции запроса 
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct EditMessageRequestOptions {
    /// (Default: false) - если значение true, то последующие сообщения пользователя не будут отображаться в чате, до тех пор пока не придет сообщения бота у которого это значение будет установлено в false. Разрешено только в личных чатах (1-1).
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub silent_response: Option<bool>,

    /// (Default: false) - если true, то меншны не будут подставляться в начало текста сообщения, а будут подставлены в соответствие с заданным форматом
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub raw_mentions: Option<bool>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct EditMessageResponse {
    /// Нет фиксированного ответа сервера. Складируем все данные сюда
    #[serde(flatten)]
    pub data: HashMap<String, String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct EditMessageError {
    /// Нет фиксированного ответа об ошибке. Складируем все данные сюда
    #[serde(flatten)]
    pub data: HashMap<String, String>,
}