use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::events::edit_event::models::*
};

/// ### /api/v3/botx/events/edit_event
/// 
/// Редактирование события
/// 
/// ### Описание
/// * Редактирование содержимого события (результата команды или нотификации)
/// * Можно редактировать только те события, что были отправлены текущим ботом
/// * Обрабатывается асинхронно
/// 
/// ### Логика обновления полей события:
/// * Если поле не указано в запросе, то оно не обновляется
/// * Для полей keyboard/bubble/mentions, если в запросе указан пустой массив [], то набор становится пустым
/// * Если обновлен текст, но при этом есть меншны, то существующие меншны будут добавлены в новый текст
/// * Если обновляется список меншнов, но есть текст, то новый список меншнов будет добавлен в текст (а старый будет удален из текста)
#[tracing::instrument(level = "debug")]
pub async fn edit_event(context: &Arc<RwLock<BotXApiContext>>, request: &EditMessageRequest) -> BotXApiResult<EditMessageResponse, EditMessageError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.edit_event_api_builder)(&context_read_lock.cts_url);
    drop(context_read_lock);

    RequestManager::post(
        url,
        request,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}