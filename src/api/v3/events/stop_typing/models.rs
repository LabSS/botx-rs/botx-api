use std::collections::HashMap;

use serde::{Serialize, Deserialize};
use uuid::Uuid;

/// Модель запроса события начала печати
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct StopTypingRequest {
    pub group_chat_id: Uuid
}

/// Модель ответа сервера на запрос события начала печати
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct StopTypingResponse {
    /// Результата события ("typing_event_pushed")
    pub result: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct StopTypingResponseError {
    /// Нет фиксированного ответа об ошибке. Складируем все данные сюда
    #[serde(flatten)]
    pub data: HashMap<String, String>,
}