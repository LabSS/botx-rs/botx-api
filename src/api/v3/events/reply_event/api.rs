use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::events::reply_event::models::*
};

/// ### /api/v3/botx/events/reply_event 	
/// 
/// Ответ (reply) на сообщение
/// 
/// ### Описание
/// * Обрабатывается асинхронно
#[tracing::instrument(level = "debug")]
pub async fn reply_event(context: &Arc<RwLock<BotXApiContext>>, request: &ReplyMessageRequest) -> BotXApiResult<ReplyMessageResult, ReplyMessageError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.reply_event_api_builder)(&context_read_lock.cts_url);
    drop(context_read_lock);

    RequestManager::post(
        url,
        request,
        RequestManager::MULTIPART_CONTENT_TYPE,
        context,
    ).await
}