use std::collections::HashMap;

use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::api::models::*;

/// Состояние сообщения ответа
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct ReplyMessageRequest {
    /// идентификатор сообщения, на который будет отправлен ответ
    pub source_sync_id: Uuid,

    /// Описание события
    pub reply: EventPayload,

    /// (Default: Skip) - файл в base64 представление. Если передать null, то существующий файл удалится из события 
    // #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub file: Option<FileWithCaption>,

    /// опции запроса
    #[builder(default)]
    pub opts: PayloadOptions,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ReplyMessageResult {
    /// Результат
    pub result: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ReplyMessageError {
    /// Нет фиксированного ответа об ошибке. Складируем все данные сюда
    #[serde(flatten)]
    pub data: HashMap<String, String>,
}