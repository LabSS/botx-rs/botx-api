use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};
use uuid::Uuid;


/// Ответ сервера на запрос состояния сообщения
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct EventStatusResponse {
    /// Результат
    pub result: EventStatusResult,
}


/// Результат запроса статуса сообщения 
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct EventStatusResult {
    /// Идентификатор чата
    pub group_chat_id: Uuid,

    /// Список идентификаторов устройств (uuid), получивших событие
    pub sent_to: Vec<Uuid>,

    /// Список пользователей, которые прочли сообщение
    pub read_by: Vec<EventReadBy>,

    /// Список пользователей, которым доставлено сообщение
    pub received_by: Vec<EventReceivedBy>,
}

/// Информация о пользователе, который прочел сообщение
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct EventReadBy {
    /// huid пользователя прочитавшего событие
    pub user_huid: Uuid,

    /// Время прочтения
    pub read_at: DateTime<Utc>,
}

/// Информация о пользователе получившем сообщение
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct EventReceivedBy {
    /// huid пользователя получившего событие
    pub user_huid: Uuid,

    /// Время получения
    pub received_at: DateTime<Utc>,
}

/// Ошибки при запросе состояния сообщения
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "reason")]
pub enum EventStatusError {
    /// Событие не найдено (404)
    #[serde(rename(serialize = "event_not_found", deserialize = "event_not_found"))]
    EventNotFound(EventNotFound),

    /// Событие не найдено (404)
    #[serde(rename(serialize = "messaging_service_error", deserialize = "messaging_service_error"))]
    MessagingServiceError(MessagingServiceError),

    // TODO: добавить десериализацию в HashMap<string, string> когда завезут реализацию
    /// Неопределенная ошибка, смотрите логи
    #[serde(other)]
    Other,
}


/// Событие не найдено (404)
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct EventNotFound {
    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: EventNotFoundData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct EventNotFoundData {
    /// uuid сообщения
    pub sync_id: Uuid
}

/// Ошибка messaging сервиса при запросе события (500)
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct MessagingServiceError {
    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: MessagingServiceErrorData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct MessagingServiceErrorData {
    /// uuid сообщения
    pub sync_id: Uuid
}