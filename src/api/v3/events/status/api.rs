use std::sync::Arc;

use async_lock::RwLock;
use uuid::Uuid;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::events::status::models::*
};

/// ### /api/v3/botx/events/:sync_id/status
/// 
/// Получение статуса сообщения
/// 
/// ### Описание
/// * Обрабатывается синхронно
#[tracing::instrument(level = "debug")]
pub async fn event_status(context: &Arc<RwLock<BotXApiContext>>, sync_id: &Uuid) -> BotXApiResult<EventStatusResponse, EventStatusError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.event_status_api_builder)(&context_read_lock.cts_url, sync_id);
    drop(context_read_lock);

    RequestManager::get(
        url,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
