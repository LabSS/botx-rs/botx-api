use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::events::typing::models::*
};

/// ### /api/v3/botx/events/typing
/// 
/// Отправка typing
/// 
/// ### Описание
/// * Обрабатывается асинхронно
#[tracing::instrument(level = "debug")]
pub async fn start_typing(context: &Arc<RwLock<BotXApiContext>>, request: &StartTypingRequest) -> BotXApiResult<StartTypingResponse, StartTypingResponseError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.typing_api_builder)(&context_read_lock.cts_url);
    drop(context_read_lock);

    RequestManager::post(
        url,
        request,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
