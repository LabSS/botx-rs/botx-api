use std::collections::HashMap;

use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::api::v3::stickers::models::StickerPack;

/// Модель запроса создания набора стикеров
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct NewStickerPackRequest {
    /// название набора стикеров
    pub name: String,

    /// (Default: null) - huid создателя набора
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub user_huid: Option<Uuid>,
}

/// Модель ответа сервера на запрос создания набора стикеров
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NewStickerPackResponse {
    /// Результата команды создания набора стикеров
    pub result: StickerPack,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NewStickerPackError {
    /// Нет фиксированного ответа об ошибке. Складируем все данные сюда
    #[serde(flatten)]
    pub data: HashMap<String, String>,
}