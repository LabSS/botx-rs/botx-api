use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::stickers::new_sticker_pack::models::*
};

/// ### /api/v3/botx/stickers/packs
/// 
/// Создание набора стикеров
/// 
/// ### Описание
/// * Обрабатывается асинхронно
/// * Создает только непубличные наборы стикеров (набор виден только на CTS с ботом)
#[tracing::instrument(level = "debug")]
pub async fn new_sticker_pack(context: &Arc<RwLock<BotXApiContext>>, request: &NewStickerPackRequest) -> BotXApiResult<NewStickerPackResponse, NewStickerPackError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.new_sticker_pack_api_builder)(&context_read_lock.cts_url);
    drop(context_read_lock);

    RequestManager::post(
        url,
        request,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
