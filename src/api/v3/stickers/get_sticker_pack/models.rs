use std::collections::HashMap;

use serde::{Serialize, Deserialize};

use crate::api::v3::stickers::models::StickerPack;


/// Модель ответа сервера на запрос информации о наборе стикеров
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct GetStickerPackResponse {
    /// Результат запроса информации о наборе стикеров
    pub result: StickerPack
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct GetStickerPackError {
    /// Нет фиксированного ответа об ошибке. Складируем все данные сюда
    #[serde(flatten)]
    pub data: HashMap<String, String>,
}