use std::collections::HashMap;

use serde::{Serialize, Deserialize};
use uuid::Uuid;

/// Модель ответа сервера на запроса информации о стикере
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct GetStickerResponse {
    /// Результат запроса информации о стикере
    pub result: StickerInfo,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct StickerInfo {
    /// id стикера
    pub id: Uuid,
    /// Эмоджи соответствующее стикеру
    pub emoji: char,
    /// Url на картинку стикера
    pub link: String,
    /// Превью стикера
    pub preview: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct GetStickerError {
    /// Нет фиксированного ответа об ошибке. Складируем все данные сюда
    #[serde(flatten)]
    pub data: HashMap<String, String>,
}