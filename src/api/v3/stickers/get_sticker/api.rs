use std::sync::Arc;

use async_lock::RwLock;
use uuid::Uuid;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::stickers::get_sticker::models::*
};

/// ### /api/v3/botx/stickers/packs/:pack_id/stickers/:sticker_id
/// 
/// Получение стикера из набора стикеров
/// 
/// ### Описание
/// * Обрабатывается асинхронно
#[tracing::instrument(level = "debug")]
pub async fn get_sticker(context: &Arc<RwLock<BotXApiContext>>, pack_id: &Uuid, sticker_id: &Uuid) -> BotXApiResult<GetStickerResponse, GetStickerError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.get_sticker_api_builder)(&context_read_lock.cts_url, pack_id, sticker_id);
    drop(context_read_lock);

    RequestManager::get(
        url,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
