use std::collections::HashMap;

use chrono::{DateTime, Utc};
use serde::{Serialize, Deserialize};
use uuid::Uuid;


/// Модель запроса добавления стикера в набор стикеров
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct AddStickerRequest {
    /// Эмодзи стикера
    pub emoji: char,

    /// Изображение стикера в base64 формате
    pub image: String,
}

/// Модель ответа на запрос добавления стикера в набор стикеров
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AddStickerResponse {
    /// Результат добавления стикера в набор стикеров
    pub result: AddStickerResponseResult,
}

/// Результат добавления нового стикера в набор стикеров
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AddStickerResponseResult {
    /// id стикера
    pub id: Uuid,

    /// Эмодзи стикера
    pub emoji: char,

    /// Url до картинки стикера
    pub link: String,

    /// Время создания стикера
    pub inserted_at: DateTime<Utc>,

    /// Время последнего обновления стикера
    pub updated_at: DateTime<Utc>,
    
    // Время удаления стикера
    pub deleted_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AddStickerError {
    /// Нет фиксированного ответа об ошибке. Складируем все данные сюда
    #[serde(flatten)]
    pub data: HashMap<String, String>,
}