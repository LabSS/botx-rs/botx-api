
use std::sync::Arc;

use async_lock::RwLock;
use uuid::Uuid;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::stickers::add_sticker::models::*
};

/// ### /api/v3/botx/stickers/packs/:pack_id/stickers
/// 
/// Добавление стикера в набор стикеров
/// 
/// ### Описание
/// * Обрабатывается асинхронно
#[tracing::instrument(level = "debug")]
pub async fn add_sticker(context: &Arc<RwLock<BotXApiContext>>, pack_id: &Uuid, request: &AddStickerRequest) -> BotXApiResult<AddStickerResponse, AddStickerError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.add_sticker_api_builder)(&context_read_lock.cts_url, pack_id);
    drop(context_read_lock);

    RequestManager::post(
        url,
        request,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
