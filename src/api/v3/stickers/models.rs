use std::char;

use chrono::{DateTime, Utc};
use serde::{Serialize, Deserialize};
use uuid::Uuid;


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct StickerPack {
    /// id наборе стикеров
    pub id: Uuid,
    /// Название наборе стикеров
    pub name: String,
    /// Признак публично доступного наборе
    pub public: bool,
    /// Url для превью наборе
    pub preview: Option<String>,
    /// Стикеры в наборе
    pub stickers: Vec<Sticker>,
    /// Порядок отображения стикеров
    pub stickers_order: Vec<Uuid>,
    /// Время создания набора
    pub inserted_at: DateTime<Utc>,
    /// Время последнего обновления набора
    pub updated_at: DateTime<Utc>,
    /// Время удаления набора
    pub deleted_at: Option<DateTime<Utc>>,
}


#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Sticker {
    /// id стикера
    pub id: Uuid,
    /// Эмоджи соответствующее стикеру
    pub emoji: char,
    /// Url на картинку стикера
    pub link: String,
    /// Время создания стикера
    pub inserted_at: DateTime<Utc>,
    /// Время последнего обновления стикера
    pub updated_at: DateTime<Utc>,
    // Время удаления стикера
    pub deleted_at: Option<DateTime<Utc>>,
}