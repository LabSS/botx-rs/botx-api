use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::stickers::sticker_packs_list::models::*
};

/// ### /api/v3/botx/stickers/packs
/// 
/// Получение списка наборов стикеров
/// 
/// ### Описание
/// * Обрабатывается асинхронно
/// * Для перемещения по списку используется пагинация
#[tracing::instrument(level = "debug")]
pub async fn sticker_packs_list(context: &Arc<RwLock<BotXApiContext>>, request: &StickerPacksListRequest) -> BotXApiResult<StickerPacksListResponse, StickerPacksListError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.sticker_packs_list_api_builder)(&context_read_lock.cts_url, &request.user_huid, &request.limit, &request.after);
    drop(context_read_lock);

    RequestManager::get(
        url,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
