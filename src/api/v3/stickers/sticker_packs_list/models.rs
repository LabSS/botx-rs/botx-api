use std::collections::HashMap;

use chrono::{DateTime, Utc};
use serde::{Serialize, Deserialize};
use uuid::Uuid;

#[derive(Debug, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct StickerPacksListRequest {
    /// Уникальный идентификатор создателя набора(ов) стикеров
    #[builder(default)]
    pub user_huid: Option<Uuid>,

    /// Максимальное количество наборов в списке
    #[builder(default)]
    pub limit: Option<u32>,

    /// Base64 строка с мета информацией для перемещения по списку. Генерируется на стороне сервера. Содержится в ответе на каждый запрос в объекте pagination (см. пример успешного ответа).
    #[builder(default)]
    pub after: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct StickerPacksListResponse {
    /// Результат запроса списка наборов стикеров
    pub result: StickerPacksListResponseResult,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct StickerPacksListResponseResult {
    /// Наборы стикеров из текущей запрошенной части коллекции (пагинация ответа)
    pub packs: Vec<StickerPackWithStickerCount>,
    /// Информация о пагинации
    pub pagination: StickerPacksListPagination,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct StickerPackWithStickerCount {
    /// id наборе стикеров
    pub id: Uuid,
    /// Название наборе стикеров
    pub name: String,
    /// Признак публично доступного наборе
    pub public: bool,
    /// Url для превью наборе
    pub preview: Option<String>,
    /// Стикеры в наборе
    pub stickers_count: u32,
    /// Порядок отображения стикеров
    pub stickers_order: Vec<Uuid>,
    /// Время создания набора
    pub inserted_at: DateTime<Utc>,
    /// Время последнего обновления набора
    pub updated_at: DateTime<Utc>,
    /// Время удаления набора
    pub deleted_at: Option<DateTime<Utc>>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct StickerPacksListPagination {
    /// Токен для итерации по наборам стикеров (пагинация ответа)
    pub after: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct StickerPacksListError {
    /// Нет фиксированного ответа об ошибке. Складируем все данные сюда
    #[serde(flatten)]
    pub data: HashMap<String, String>,
}