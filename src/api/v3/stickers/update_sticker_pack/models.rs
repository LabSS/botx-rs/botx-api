use std::collections::HashMap;

use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::api::v3::stickers::models::StickerPack;

/// Модель запроса обновления набора стикеров
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct UpdateStickerPackRequest {
    /// Название набора стикеров
    pub name: String,
    /// Уникальный идентификатор стикера из набора, выбранного в качестве превью
    pub preview: Uuid,
    /// Список идентификаторов стикеров набора в порядке их отображения. Для изменения порядка необходимо передавать весь список идентификаторов, в противном случае параметр игнорируется
    #[builder(default)]
    pub stickers_order: Vec<Uuid>,
}

/// Ответ eXpress на обновление набора стикеров
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct UpdateStickerPackResponse {
    /// Результат обновления набора стикеров
    pub result: StickerPack,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct UpdateStickerPackError {
    /// Нет фиксированного ответа об ошибке. Складируем все данные сюда
    #[serde(flatten)]
    pub data: HashMap<String, String>,
}