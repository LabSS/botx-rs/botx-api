use std::sync::Arc;

use async_lock::RwLock;
use uuid::Uuid;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::stickers::delete_sticker_pack::models::*
};

/// ### /api/v3/botx/stickers/packs/:pack_id
/// 
/// Удаление набора стикеров
/// 
/// ### Описание
/// * Обрабатывается асинхронно
#[tracing::instrument(level = "debug")]
pub async fn delete_sticker_pack(context: &Arc<RwLock<BotXApiContext>>, pack_id: &Uuid) -> BotXApiResult<DeleteStickerPackResponse, DeleteStickerPackError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.delete_sticker_pack_api_builder)(&context_read_lock.cts_url, pack_id);
    drop(context_read_lock);

    RequestManager::delete(
        url,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
