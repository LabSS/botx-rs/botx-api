use std::collections::HashMap;

use serde::{Serialize, Deserialize};

/// Ответ eXpress на запрос удаления набора стикеров
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DeleteStickerPackResponse {
    /// Результат запроса удаления набора стикеров
    pub result: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DeleteStickerPackError {
    /// Нет фиксированного ответа об ошибке. Складируем все данные сюда
    #[serde(flatten)]
    pub data: HashMap<String, String>,
}