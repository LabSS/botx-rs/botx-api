use std::collections::HashMap;

use serde::{Serialize, Deserialize};

/// Ответ eXpress на запрос удаления стикера
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DeleteStickerResponse {
    /// Результат запроса удаления стикера
    pub result: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DeleteStickerError {
    /// Нет фиксированного ответа об ошибке. Складируем все данные сюда
    #[serde(flatten)]
    pub data: HashMap<String, String>,
}