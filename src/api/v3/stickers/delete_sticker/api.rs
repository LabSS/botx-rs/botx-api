use std::sync::Arc;

use async_lock::RwLock;
use uuid::Uuid;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::stickers::delete_sticker::models::*
};

/// ### /api/v3/botx/stickers/packs/:pack_id/stickers/:sticker_id
/// 
/// Удаление стикера из набора стикеров
/// 
/// ### Описание
/// * Обрабатывается асинхронно
#[tracing::instrument(level = "debug")]
pub async fn delete_sticker(context: &Arc<RwLock<BotXApiContext>>, pack_id: &Uuid, sticker_id: &Uuid) -> BotXApiResult<DeleteStickerResponse, DeleteStickerError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.delete_sticker_api_builder)(&context_read_lock.cts_url, pack_id, sticker_id);
    drop(context_read_lock);

    RequestManager::delete(
        url,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
