use chrono::{DateTime, Utc};
use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::bot::models::UserKind;

/// Информация о пользователе
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct UserInfo {
    /// id пользователя в системе eXpress
    pub user_huid: Uuid,

    /// active-directory логин пользователя
    pub ad_login: String,

    /// active-directory домен
    pub ad_domain: String,

    /// Имя пользователя
    pub name: String,

    /// Компания которую представляет пользователь
    pub company: Option<String>,

    /// Позиция пользователя в компании
    pub company_position: Option<String>,

    /// Департамент в составе которого находится пользователь
    pub department: Option<String>,

    /// Email адреса пользователя
    pub emails: Vec<String>,

    /// Альтернативный id?
    /// 
    /// TODO: откройте issue с расширением описания если станет понятно что это
    pub other_id: String,

    /// Вид пользователя
    pub user_kind: UserKind,

    /// Признак активности пользователя
    pub active: Option<bool>,

    /// Время регистрации пользователя
    pub created_at: Option<DateTime<Utc>>,

    /// id сервера на котором зарегистрирован пользователь
    pub cts_id: Option<Uuid>,

    /// Описание пользователя
    /// 
    /// TODO: откройте issue с расширением описания если станет понятно для чего это используется
    pub description: Option<String>,

    /// ip для внутренних звонков eXpress
    pub ip_phone: Option<u32>,

    /// Начальник пользователя?
    /// 
    /// TODO: откройте issue с расширением описания если станет понятно для чего это используется
    pub manager: Option<String>,

    /// Офис пользователя?
    /// 
    /// TODO: откройте issue с расширением описания если станет понятно для чего это используется
    pub office: Option<String>,

    /// Альтернативный ip для внутренних звонков eXpress
    /// 
    /// TODO: откройте issue с расширением описания если станет понятно для чего это используется
    pub other_ip_phone: Option<u32>,

    /// Альтернативный телефон пользователя
    /// 
    /// TODO: утонить где основной, добавить примечание
    pub other_phone: Option<String>,

    /// Публичное имя пользователя
    /// 
    /// TODO: откройте issue с расширением описания если станет понятно для чего это используется
    pub public_name: Option<String>,

    /// TODO: откройте issue с расширением описания если станет понятно для чего это используется
    pub rts_id: Option<Uuid>,

    /// Время последнего изменения информации о пользователе 
    pub updated_at: Option<DateTime<Utc>>,
}

/// Данные юзера не найдены
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct UserNotFound {
    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: UserNotFoundData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct UserNotFoundData {
}