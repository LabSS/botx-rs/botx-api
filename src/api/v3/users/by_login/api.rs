use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::users::by_login::models::*
};

/// ### /api/v3/botx/users/by_login
/// 
/// Поиск данных юзера по его ad логину и ad домену
/// 
/// ### Описание
/// * Обрабатывается синхронно
#[tracing::instrument(level = "debug")]
pub async fn get_user_by_login(context: &Arc<RwLock<BotXApiContext>>, login: &String, domain: &String) -> BotXApiResult<GetUserByLoginResponse, GetUserByLoginError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.get_user_by_login_api_builder)(&context_read_lock.cts_url, login, domain);
    drop(context_read_lock);

    RequestManager::get(
        url,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
