
use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::users::by_email::models::*
};

/// ### /api/v3/botx/users/by_email
/// 
/// Поиск данных юзеров по их почтам
/// 
/// ### Описание
/// * Обрабатывается синхронно
#[tracing::instrument(level = "debug")]
pub async fn get_users_by_email(context: &Arc<RwLock<BotXApiContext>>, request: &GetUsersByEmailRequest) -> BotXApiResult<GetUsersByEmailResponse, GetUsersByEmailError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.get_users_by_email_api_builder)(&context_read_lock.cts_url);
    drop(context_read_lock);

    RequestManager::post(
        url,
        request,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
