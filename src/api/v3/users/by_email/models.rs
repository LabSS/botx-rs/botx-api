use std::collections::HashMap;

use serde::{Serialize, Deserialize};

use crate::api::v3::users::models::UserInfo;

/// Запрос информации о пользователях по email
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct GetUsersByEmailRequest {
    /// почты юзеров
    pub emails: Vec<String>,
}

/// Ответ eXpress на запрос информации о пользователях по email
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct GetUsersByEmailResponse {
    /// Результат запроса
    pub result: Vec<UserInfo>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct GetUsersByEmailError {
    /// Нет фиксированного ответа об ошибке. Складируем все данные сюда
    #[serde(flatten)]
    pub data: HashMap<String, String>,
}