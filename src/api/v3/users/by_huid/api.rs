use std::sync::Arc;

use async_lock::RwLock;
use uuid::Uuid;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::users::by_huid::models::*
};

/// ### /api/v3/botx/users/by_huid
/// 
/// Поиск данных юзера по его huid
/// 
/// ### Описание
/// * Обрабатывается синхронно
#[tracing::instrument(level = "debug")]
pub async fn get_user_by_huid(context: &Arc<RwLock<BotXApiContext>>, huid: &Uuid) -> BotXApiResult<GetUserByHuidResponse, GetUserByHuidError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.get_user_by_huid_api_builder)(&context_read_lock.cts_url, huid);
    drop(context_read_lock);

    RequestManager::get(
        url,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
