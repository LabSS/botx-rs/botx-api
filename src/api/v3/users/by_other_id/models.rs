use serde::{Serialize, Deserialize};

use crate::api::v3::users::models::{UserInfo, UserNotFound};

/// Ответ eXpress на запрос информации о пользователе по other_id
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct GetUserByOtherIdResponse {
    /// Результат запроса
    pub result: UserInfo,
}

/// Ошибки при получении информации о пользователе по other_id
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "reason")]
pub enum GetUserByOtherIdError {
    /// Чат не найден
    #[serde(rename(serialize = "user_not_found", deserialize = "user_not_found"))]
    UserNotFound(UserNotFound),

    // TODO: добавить десериализацию в HashMap<string, string> когда завезут реализацию
    /// Неопределенная ошибка, смотрите логи
    #[serde(other)]
    Other,
}