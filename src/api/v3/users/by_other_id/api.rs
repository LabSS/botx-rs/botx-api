use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::users::by_other_id::models::*
};

/// ### /api/v3/botx/users/by_other_id
/// 
/// Поиск данных юзера по дополнительному идентификатору
/// 
/// ### Описание
/// * Обрабатывается синхронно
#[tracing::instrument(level = "debug")]
pub async fn get_user_by_other_id(context: &Arc<RwLock<BotXApiContext>>, other_id: &String) -> BotXApiResult<GetUserByOtherIdResponse, GetUserByOtherIdError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.get_user_by_other_id_api_builder)(&context_read_lock.cts_url, other_id);
    drop(context_read_lock);

    RequestManager::get(
        url,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
