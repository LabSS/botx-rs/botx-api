use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::bot::models::UserKind;

/// Источник регистрации пользователя
#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum SyncSource {
    #[serde(rename(serialize = "ad", deserialize = "ad"))]
    Ad,
    #[serde(rename(serialize = "admin", deserialize = "admin"))]
    Admin,
    #[serde(rename(serialize = "email", deserialize = "email"))]
    Email,
    #[serde(rename(serialize = "openid", deserialize = "openid"))]
    OpenId,
}

/// Информация о пользователе (укороченная)
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct UserShortInfo {
    #[serde(rename = "HUID")]
    pub huid: Uuid,
    
    #[serde(rename = "AD Login")]
    pub ad_login: String,

    #[serde(rename = "Domain")]
    pub ad_domain: String,

    #[serde(rename = "AD E-mail")]
    pub ad_email: Option<String>,

    #[serde(rename = "Name")]
    pub name: String,

    #[serde(rename = "Sync source")]
    pub sync_source: SyncSource,

    #[serde(rename = "Active")]
    pub active: bool,

    #[serde(rename = "Kind")]
    pub kind: UserKind,

    #[serde(rename = "Company")]
    pub company: Option<String>,

    #[serde(rename = "Department")]
    pub department: Option<String>,

    #[serde(rename = "Position")]
    pub position: Option<String>,
}

/// Ошибки при получении списка пользователей
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "reason")]
pub enum GetUsersAsCsvError {
    /// Не выбран ни один из типов пользователей
    #[serde(rename(serialize = "no_user_kind_selected", deserialize = "no_user_kind_selected"))]
    NoUserKindSelected(NoUserKindSelected),

    // TODO: добавить десериализацию в HashMap<string, string> когда завезут реализацию
    /// Неопределенная ошибка, смотрите логи
    #[serde(other)]
    Other,
}

/// Не выбран ни один из типов пользователей
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NoUserKindSelected {
    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: NoUserKindSelectedData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NoUserKindSelectedData {
}