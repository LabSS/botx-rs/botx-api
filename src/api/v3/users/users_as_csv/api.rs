use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::users::users_as_csv::models::*
};

/// ### /api/v3/botx/users/by_other_id
/// 
/// Поиск данных юзера по дополнительному идентификатору
/// 
/// ### Описание
/// * Обрабатывается синхронно
#[tracing::instrument(level = "debug")]
pub async fn get_users_as_csv(context: &Arc<RwLock<BotXApiContext>>, cts_user: bool, unregistered: bool, botx: bool) -> BotXApiResult<Vec<UserShortInfo>, GetUsersAsCsvError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.get_users_as_csv_api_builder)(&context_read_lock.cts_url, cts_user, unregistered, botx);
    drop(context_read_lock);

    RequestManager::get_csv(
        url,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
