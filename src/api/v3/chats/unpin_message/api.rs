
use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::chats::unpin_message::models::*
};

/// ### /api/v3/botx/chats/unpin_message
/// 
/// Открепление сообщения в чате
/// 
/// ### Описание
/// * Обрабатывается синхронно
/// * Бот открепляет указанное сообщение в чате. Для открепления в каналах у бота должны быть права администратора.
#[tracing::instrument(level = "debug")]
pub async fn unpin_message(context: &Arc<RwLock<BotXApiContext>>, request: &UnpinMessageRequest) -> BotXApiResult<UnpinMessageResponse, UnpinMessageError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.unpin_message_api_builder)(&context_read_lock.cts_url);
    drop(context_read_lock);

    RequestManager::post(
        url,
        request,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
