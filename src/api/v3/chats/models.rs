use serde::{Serialize, Deserialize};
use uuid::Uuid;

/// Нет доступа для операции
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NoPermissionForPinOperation {
    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: NoPermissionForPinOperationData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NoPermissionForPinOperationData {
    /// id чата 
    pub group_chat_id: Uuid,

    /// id отправителя события
    pub bot_id: Uuid,

    /// Описание ошибки
    pub error_description: String,
}

/// Нет доступа для операции
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NoPermissionForOperation {
    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: NoPermissionForOperationData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct NoPermissionForOperationData {
    /// id чата 
    pub group_chat_id: Uuid,

    /// id отправителя события
    pub sender: Uuid,
}

/// Редактирование участников чата запрещено
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ChatMemberNotModifiableWithChatId {
    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: ChatMemberNotModifiableWithChatIdData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ChatMemberNotModifiableWithChatIdData {
    /// id чата 
    pub group_chat_id: Uuid,
}

/// Редактирование участников чата запрещено
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ChatMemberNotModifiable {
    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: ChatMemberNotModifiableData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ChatMemberNotModifiableData {
}