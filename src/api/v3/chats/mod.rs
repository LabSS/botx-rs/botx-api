#[cfg(feature = "chats-list")]
pub mod list;

#[cfg(feature = "chats-info")]
pub mod info;

#[cfg(feature = "chats-add_user")]
pub mod add_user;

#[cfg(feature = "chats-remove_user")]
pub mod remove_user;
#[cfg(feature = "chats-add_admin")]
pub mod add_admin;

#[cfg(feature = "chats-set_stealth")]
pub mod set_stealth;

#[cfg(feature = "chats-disable_stealth")]
pub mod disable_stealth;

#[cfg(feature = "chats-create")]
pub mod create;

#[cfg(feature = "chats-pin_message")]
pub mod pin_message;

#[cfg(feature = "chats-unpin_message")]
pub mod unpin_message;

pub mod models;
