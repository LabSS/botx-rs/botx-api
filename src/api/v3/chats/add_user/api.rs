
use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::chats::add_user::models::*
};

/// ### /api/v3/botx/chats/add_user
/// 
/// Добавление юзеров в чат
/// 
/// ### Описание
/// * Обрабатывается синхронно
#[tracing::instrument(level = "debug")]
pub async fn add_user(context: &Arc<RwLock<BotXApiContext>>, request: &AddUserRequest) -> BotXApiResult<AddUserResponse, AddUserError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.add_user_api_builder)(&context_read_lock.cts_url);
    drop(context_read_lock);

    RequestManager::post(
        url,
        request,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
