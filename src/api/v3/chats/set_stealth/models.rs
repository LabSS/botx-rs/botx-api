use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::api::{
    models::ChatNotFound,
    v3::chats::models::*
};

/// Запрос на включении стелс режима в чате
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct SetStealthRequest {
    /// Идентификатор чата
    pub group_chat_id: Uuid,

    /// (Default: false) - если true - отключает доступ к чату с веб-клиента (по умолчанию false)
    pub disable_web: bool,

    /// (Default: null) - Время сгорания для прочитавшего, в секундах (по-умолчанию null - выключено)
    #[builder(default)]
    pub burn_in: Option<u32>,

    /// (Default: null) - время сгорания для всех участников чата, в секундах (по-умолчанию null - выключено)
    #[builder(default)]
    pub expire_in: Option<u32>,
}

/// Ответ eXpress на включении стелс режима в чате
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SetStealthResponse {
    /// Результат запроса
    /// 
    /// TODO: Я не знаю что значит false, документация не описывает, откройте issue как станет понятно 
    pub result: bool,
}

/// Ошибки при включении стелс режима в чате
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "reason")]
pub enum SetStealthError {
    /// Чат не найден
    #[serde(rename(serialize = "chat_not_found", deserialize = "chat_not_found"))]
    ChatNotFound(ChatNotFound),

    /// Бот не является админом чата
    #[serde(rename(serialize = "no_permission_for_operation", deserialize = "no_permission_for_operation"))]
    NoPermissionForOperation(NoPermissionForOperation),

    // TODO: добавить десериализацию в HashMap<string, string> когда завезут реализацию
    /// Неопределенная ошибка, смотрите логи
    #[serde(other)]
    Other,
}