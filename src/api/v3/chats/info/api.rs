
use std::sync::Arc;

use async_lock::RwLock;
use uuid::Uuid;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::chats::info::models::*
};

/// ### /api/v3/botx/chats/info
/// 
/// Получение информации о чате
/// 
/// ### Описание
/// * Обрабатывается синхронно
#[tracing::instrument(level = "debug")]
pub async fn get_chat_info(context: &Arc<RwLock<BotXApiContext>>, group_chat_id: &Uuid) -> BotXApiResult<ChatInfoResponse, ChatInfoError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.get_chat_info_api_builder)(&context_read_lock.cts_url, group_chat_id);
    drop(context_read_lock);

    RequestManager::get(
        url,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
