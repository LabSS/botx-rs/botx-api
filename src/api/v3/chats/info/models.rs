use chrono::{DateTime, Utc};
use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::{bot::models::{ChatType, UserKind}, api::models::{ChatNotFound, ErrorFromMessagingService}};

/// Ответ eXpress на запрос информации о чате
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ChatInfoResponse {
    /// Результат запроса
    pub result: ChatInfoResult,
}

/// Результат обработки запроса информации о чате
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ChatInfoResult {
    pub chat_type: ChatType,
    pub creator: Uuid,
    pub description: Option<String>,
    pub group_chat_id: Uuid,
    pub inserted_at: DateTime<Utc>,
    pub members: Vec<ChatMember>,
    pub name: String,
    pub shared_history: bool,
}

/// Информация об участнике чата
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ChatMember {
    /// Признак администратора
    pub admin: bool,

    /// id сервера на котором авторизован пользователь?
    pub server_id: Uuid,

    /// id пользователя в системе eXpress
    pub user_huid: Uuid,

    /// Тип пользователя
    pub user_kind: UserKind,
}

/// Ошибки при запросе информации о чате
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "reason")]
pub enum ChatInfoError {
    /// Чат не найден
    #[serde(rename(serialize = "chat_not_found", deserialize = "chat_not_found"))]
    ChatNotFound(ChatNotFound),

    /// Ошибка от Messaging сервиса
    #[serde(rename(serialize = "error_from_messaging_service", deserialize = "error_from_messaging_service"))]
    ErrorFromMessagingService(ErrorFromMessagingService),

    // TODO: добавить десериализацию в HashMap<string, string> когда завезут реализацию
    /// Неопределенная ошибка, смотрите логи
    #[serde(other)]
    Other,
}