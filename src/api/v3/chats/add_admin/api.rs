
use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::chats::add_admin::models::*
};

/// ### /api/v3/botx/chats/add_admin
/// 
/// Добавление администратора в чат
/// 
/// ### Описание
/// * Обрабатывается синхронно
#[tracing::instrument(level = "debug")]
pub async fn add_admin(context: &Arc<RwLock<BotXApiContext>>, request: &AddAdminRequest) -> BotXApiResult<AddAdminResponse, AddAdminError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.add_admin_api_builder)(&context_read_lock.cts_url);
    drop(context_read_lock);

    RequestManager::post(
        url,
        request,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
