use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::bot::models::ChatType;

/// Запрос на создания чате
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct CreateChatRequest {
    /// Имя чата
    pub name : String,

    /// (Default: null) - описание чата
    #[builder(default)]
    pub description: Option<String>,

    /// Тип чата (chat|group_chat|channel)
    pub chat_type: ChatType,

    /// Список HUID участников чата
    pub members: Vec<Uuid>,

    /// (Default: null) - аватар чата в формате data URL + base64 data (RFC 2397)
    #[builder(default)]
    pub avatar: Option<String>,

    /// (Default: false) - если true, то созданный чат будет иметь признаки шаред чата
    pub shared_history: bool,
}

/// Ответ eXpress на запрос создания чате
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct CreateChatResponse {
    /// Результат запроса
    pub result: CreateChatResult,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct CreateChatResult {
    /// id созданного чата
    pub chat_id: Uuid,
}

/// Ошибки при создании чате
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "reason")]
pub enum CreateChatError {
    /// Боту запрещено создавать чаты
    #[serde(rename(serialize = "chat_not_found", deserialize = "chat_not_found"))]
    ChatCreationIsProhibited(ChatCreationIsProhibited),

    // TODO: добавить десериализацию в HashMap<string, string> когда завезут реализацию
    /// Неопределенная ошибка, смотрите логи
    #[serde(other)]
    Other,
}

/// Боту запрещено создавать чаты
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ChatCreationIsProhibited {
    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: ChatCreationIsProhibitedData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ChatCreationIsProhibitedData {
    /// id бота 
    pub bot_id: Uuid,
}