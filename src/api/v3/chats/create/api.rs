
use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::chats::create::models::*
};

/// ### /api/v3/botx/chats/create
/// 
/// Создание чата
/// 
/// ### Описание
/// * Обрабатывается синхронно
/// * Создателем чата будет являться бот, который инициировал запрос
/// * Бот будет добавлен в список участников чата, даже если он явно не указан в списке при отправке запроса
/// * Чтобы бот мог создавать чат, в панели администратора ему нужно задать свойство allow_chat_creating в значение true
#[tracing::instrument(level = "debug")]
pub async fn create_chat(context: &Arc<RwLock<BotXApiContext>>, request: &CreateChatRequest) -> BotXApiResult<CreateChatResponse, CreateChatError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.create_chat_api_builder)(&context_read_lock.cts_url);
    drop(context_read_lock);

    RequestManager::post(
        url,
        request,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
