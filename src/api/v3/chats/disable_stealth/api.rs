
use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::chats::disable_stealth::models::*
};

/// ### /api/v3/botx/chats/stealth_disable
/// 
/// Отключение стелс режима в чате
/// 
/// ### Описание
/// * Обрабатывается синхронно
#[tracing::instrument(level = "debug")]
pub async fn disable_stealth(context: &Arc<RwLock<BotXApiContext>>, request: &DisableStealthRequest) -> BotXApiResult<DisableStealthResponse, DisableStealthError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.disable_stealth_api_builder)(&context_read_lock.cts_url);
    drop(context_read_lock);

    RequestManager::post(
        url,
        request,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
