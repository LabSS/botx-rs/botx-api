use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::api::{
    models::ChatNotFound,
    v3::chats::models::*
};

/// Запрос на отключении стелс режима в чате
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct DisableStealthRequest {
    /// Идентификатор чата
    pub group_chat_id: Uuid,
}

/// Ответ eXpress на запрос отключении стелс режима в чате
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct DisableStealthResponse {
    /// Результат запроса
    /// 
    /// TODO: Я не знаю что значит false, документация не описывает, откройте issue как станет понятно 
    pub result: bool,
}

/// Ошибки при отключении стелс режима в чате
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "reason")]
pub enum DisableStealthError {
    /// Чат не найден
    #[serde(rename(serialize = "chat_not_found", deserialize = "chat_not_found"))]
    ChatNotFound(ChatNotFound),

    /// Бот не является админом чата
    #[serde(rename(serialize = "no_permission_for_operation", deserialize = "no_permission_for_operation"))]
    NoPermissionForOperation(NoPermissionForOperation),

    // TODO: добавить десериализацию в HashMap<string, string> когда завезут реализацию
    /// Неопределенная ошибка, смотрите логи
    #[serde(other)]
    Other,
}