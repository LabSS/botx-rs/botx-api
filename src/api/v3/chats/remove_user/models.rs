use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::api::{
    models::ChatNotFound,
    v3::chats::models::*
};


/// Запрос на удаления пользователей из чата
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct RemoveUserRequest {
    /// Идентификатор чата
    pub group_chat_id: Uuid,
    
    /// Список удаляемых пользователей
    pub user_huids: Vec<Uuid>,
}

/// Ответ eXpress на запрос удаления пользователей из чата
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct RemoveUserResponse {
    /// Результат запроса
    /// 
    /// TODO: Я не знаю что значит false, документация не описывает, откройте issue как станет понятно 
    pub result: bool,
}

/// Ошибки при удалении пользователей из чата
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "reason")]
pub enum RemoveUserError {
    /// Чат не найден
    #[serde(rename(serialize = "chat_not_found", deserialize = "chat_not_found"))]
    ChatNotFound(ChatNotFound),

    /// Бот не является админом чата
    #[serde(rename(serialize = "no_permission_for_operation", deserialize = "no_permission_for_operation"))]
    NoPermissionForOperation(NoPermissionForOperation),

    /// Редактирование списка участников персонального чата невозможно
    #[serde(rename(serialize = "chat_members_not_modifiable", deserialize = "chat_members_not_modifiable"))]
    ChatMemberNotModifiable(ChatMemberNotModifiableWithChatId),

    // TODO: добавить десериализацию в HashMap<string, string> когда завезут реализацию
    /// Неопределенная ошибка, смотрите логи
    #[serde(other)]
    Other,
}