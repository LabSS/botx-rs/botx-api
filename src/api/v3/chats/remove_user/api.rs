
use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::chats::remove_user::models::*
};

/// ### /api/v3/botx/chats/remove_user
/// 
/// Удаление юзеров из чата
/// 
/// ### Описание
/// * Обрабатывается синхронно
#[tracing::instrument(level = "debug")]
pub async fn remove_user(context: &Arc<RwLock<BotXApiContext>>, request: &RemoveUserRequest) -> BotXApiResult<RemoveUserResponse, RemoveUserError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.remove_user_api_builder)(&context_read_lock.cts_url);
    drop(context_read_lock);

    RequestManager::post(
        url,
        request,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
