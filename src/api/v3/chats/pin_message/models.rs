use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::api::{
    models::*,
    v3::chats::models::*
};

/// Запрос на закрепление сообщения в чате
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct PinMessageRequest {
    /// Идентификатор чата
    pub chat_id: Uuid,
    
    /// Идентификатор сообщения
    pub sync_id: Uuid,
}

/// Ответ eXpress на запрос закрепления сообщения в чате
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct PinMessageResponse {
    /// Результат запроса
    /// 
    /// TODO: Я не знаю зачем тут строка, всегда должно быть "message_pinned" документация не описывает других вариантов, откройте issue если станет понятно что оно имеет несколько значений чтобы заменить на enum
    pub result: String,
}

/// Ошибки при закреплении сообщения в чате
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "reason")]
pub enum PinMessageError {
    /// Чат не найден
    #[serde(rename(serialize = "chat_not_found", deserialize = "chat_not_found"))]
    ChatNotFound(ChatNotFound),

    /// Бот не может закрепить сообщение (скорее всего не админ)
    #[serde(rename(serialize = "no_permission_for_operation", deserialize = "no_permission_for_operation"))]
    NoPermissionForOperation(NoPermissionForPinOperation),

    /// Ошибка от Messaging сервиса
    #[serde(rename(serialize = "error_from_messaging_service", deserialize = "error_from_messaging_service"))]
    ErrorFromMessagingService(ErrorFromMessagingService),

    // TODO: добавить десериализацию в HashMap<string, string> когда завезут реализацию
    /// Неопределенная ошибка, смотрите логи
    #[serde(other)]
    Other,
}