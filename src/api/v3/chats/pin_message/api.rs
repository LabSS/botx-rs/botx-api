
use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::chats::pin_message::models::*
};

/// ### /api/v3/botx/chats/pin_message
/// 
/// Закрепление сообщения в чате
/// 
/// ### Описание
/// * Обрабатывается синхронно
/// * Бот закрепляет указанное сообщение в чате. Для закрепления в каналах у бота должны быть права администратора.
#[tracing::instrument(level = "debug")]
pub async fn pin_message(context: &Arc<RwLock<BotXApiContext>>, request: &PinMessageRequest) -> BotXApiResult<PinMessageResponse, PinMessageError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.pin_message_api_builder)(&context_read_lock.cts_url);
    drop(context_read_lock);

    RequestManager::post(
        url,
        request,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
