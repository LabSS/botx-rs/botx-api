use std::collections::HashMap;

use chrono::{DateTime, Utc};
use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::bot::models::ChatType;

/// Ответ eXpress на запрос списка чатов бота
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ChatsListResponse {
    /// Результат запроса
    pub result: Vec<ChatsListResult>,
}

/// Результат обработки запроса списка чатов
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ChatsListResult {
    /// id чата
    pub group_chat_id: Uuid,
    
    /// Тип чата
    pub chat_type: ChatType,
    
    /// Название чата
    pub name: String,
    
    /// Описание чата
    pub description: String,
    
    /// Участники чата
    pub members: Vec<Uuid>,
    
    /// Признак общей истории в чате
    pub shared_history: bool,
    
    /// Время создания чата
    pub inserted_at: DateTime<Utc>,

    /// Время последнего обновления чата
    pub updated_at: DateTime<Utc>,
}

/// Ответ с ошибкой eXpress на запрос списка чатов
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ChatsListError {
    /// Нет фиксированного ответа об ошибке. Складируем все данные сюда
    #[serde(flatten)]
    pub data: HashMap<String, String>,
}