use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::{RequestManager, MultipartPartData, MultipartPartDataContent},
    v3::files::upload_file::models::*
};

/// ### /api/v3/botx/files/upload
/// 
/// Загрузка файла
/// 
/// ### Описание
/// * Обрабатывается асинхронно
/// * Содержимое файла и параметры передается в формате multipart/form-data с boundary
#[tracing::instrument(level = "debug")]
pub async fn upload_file(context: &Arc<RwLock<BotXApiContext>>, request: &UploadRequest) -> BotXApiResult<UploadResponse, serde_json::Value> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.upload_file_api_builder)(&context_read_lock.cts_url);
    drop(context_read_lock);

    let data = vec![
        MultipartPartData {
            name: "content".to_string(),
            file_name: Some(request.file_name.clone()),
            content: MultipartPartDataContent::Bytes(request.content.clone()),
            mime_type: Some(request.mime_type.clone())
        },
        MultipartPartData {
            name: "group_chat_id".to_string(),
            file_name: None,
            content: MultipartPartDataContent::Text(request.group_chat_id.clone().to_string()),
            mime_type: None
        },
        MultipartPartData {
            name: "meta".to_string(),
            file_name: None,
            content: MultipartPartDataContent::Text(serde_json::to_string(&request.meta.clone()).unwrap()),
            mime_type: Some(RequestManager::JSON_CONTENT_TYPE.to_string())
        },
    ];

    RequestManager::post_multipart(
        url,
        data,
        context,
    ).await
}
