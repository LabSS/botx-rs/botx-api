use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::api::models::AsyncFile;

#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct UploadRequest {
    /// Идентификатор чата
    pub group_chat_id: Uuid,

    /// Имя файла. Берется из content
    pub file_name: String,

    /// mime тип файла. Берется из content
    pub mime_type: String,

    /// Бинарное содержимое файла
    pub content: Vec<u8>,

    /// Метаданные файла 
    pub meta: FileMeta,
}

#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct FileMeta {
    /// (Default: null) - длительность видео/аудио в секундах
    pub duration: Option<u128>,

    /// (Default: null) - caption файла
    pub captions: Option<String>,
}

/// Модель ответа сервера на запрос на загрузку файла
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct UploadResponse {
    /// Результат запроса на загрузку файла
    pub result: AsyncFile
}