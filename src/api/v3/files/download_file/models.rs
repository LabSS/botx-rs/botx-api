use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::api::models::*;

#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct DownloadFileRequest {
    /// Идентификатор чата
    pub group_chat_id: Uuid,

    /// Идентификатор файла
    pub file_id: Uuid,

    /// Если true и файл имеет preview, то вернется содержимое preview, иначе оригинал
    pub is_preview: bool,
}

/// Ошибки при запросе файла
#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "reason")]
pub enum DownloadFileError {
    /// Чат не найден
    #[serde(rename(serialize = "chat_not_found", deserialize = "chat_not_found"))]
    ChatNotFound(ChatNotFound),

    /// Ошибка от Messaging сервиса
    #[serde(rename(serialize = "error_from_messaging_service", deserialize = "error_from_messaging_service"))]
    ErrorFromMessagingService(ErrorFromMessagingService),

    /// Метаданные файла не найдены
    #[serde(rename(serialize = "file_metadata_not_found", deserialize = "file_metadata_not_found"))]
    FileMetadataNotFound(FileMetadataNotFound),

    /// Файл сервис вернул ошибку при запросе метаданных файла
    #[serde(rename(serialize = "error_from_file_service", deserialize = "error_from_file_service"))]
    ErrorFromFileService(ErrorFromFileService),

    /// Файл удален
    #[serde(rename(serialize = "file_deleted", deserialize = "file_deleted"))]
    FileDeleted(FileDeleted),

    /// Ошибка при скачивание файла
    #[serde(rename(serialize = "file_download_failed", deserialize = "file_download_failed"))]
    FileDownloadFailed(FileDownloadFailed),

    /// Ошибка при скачивание файла из s3 файл сервиса
    #[serde(rename(serialize = "error_from_s3_file_service", deserialize = "error_from_s3_file_service"))]
    ErrorFromS3FileService(ErrorFromS3FileService),

    /// Файл не имеет preview
    #[serde(rename(serialize = "file_without_preview", deserialize = "file_without_preview"))]
    FileWithoutPreview(FileWithoutPreview),

    // TODO: добавить десериализацию в HashMap<string, string> когда завезут реализацию
    /// Неопределенная ошибка, смотрите логи
    #[serde(other)]
    Other,
}

/// Метаданные файла не найдены
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FileMetadataNotFound {
    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: FileMetadataNotFoundData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FileMetadataNotFoundData {
    /// id запрошенного файла
    pub file_id: Uuid,
    
    /// id чата в котором происходил поиск файла
    pub group_chat_id: Uuid,

    /// Описание ошибки
    pub error_description: String,
}

/// Файл сервис вернул ошибку при запросе метаданных файла
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ErrorFromFileService {
    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: ErrorFromFileServiceData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ErrorFromFileServiceData {
    /// id запрошенного файла
    pub file_id: Uuid,
    
    /// id чата в котором происходил поиск файла
    pub group_chat_id: Uuid,

    /// Причина ошибки
    pub reason: String,

    /// Описание ошибки
    pub error_description: String,
}

/// Файл удален
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FileDeleted {
    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: FileDeletedData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FileDeletedData {
    /// Ссылка на удаленный файл
    pub link: String,

    /// Описание ошибки
    pub error_description: String,
}

/// Ошибка при скачивание файла
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FileDownloadFailed {
    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: FileDownloadFailedData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FileDownloadFailedData {
    /// Ссылка на файл
    pub link: String,

    /// Описание ошибки
    pub error_description: String,
}

/// Ошибка при скачивание файла из s3 файл сервиса
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ErrorFromS3FileService {
    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: ErrorFromS3FileServiceData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ErrorFromS3FileServiceData {
    /// Ссылка на файл
    pub source_link: String,

    /// Адрес расположения файла
    pub location: String,

    /// Описание ошибки
    pub error_description: String,
}

/// Файл не имеет preview
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FileWithoutPreview {
    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: FileWithoutPreviewData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct FileWithoutPreviewData {
    /// id запрошенного файла
    pub file_id: Uuid,
    
    /// id чата в котором происходил поиск файла
    pub group_chat_id: Uuid,

    /// Описание ошибки
    pub error_description: String,
}