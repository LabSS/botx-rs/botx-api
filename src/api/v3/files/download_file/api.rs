use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager,
    v3::files::download_file::models::*,
    models::FileResponse
};

/// ### /api/v3/botx/files/download
/// 
/// Скачивание файла
/// 
/// ### Описание
/// * Обрабатывается асинхронно
#[tracing::instrument(level = "debug")]
pub async fn download_file(context: &Arc<RwLock<BotXApiContext>>, request: &DownloadFileRequest) -> BotXApiResult<FileResponse, DownloadFileError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.download_file_api_builder)(&context_read_lock.cts_url, &request.group_chat_id, &request.file_id, &request.is_preview);
    drop(context_read_lock);

    RequestManager::get_file(
        url,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
