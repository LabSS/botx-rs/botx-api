use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{utils::request_manager::RequestManager, context::BotXApiContext, result::BotXApiResult};

use crate::api::v3::smartapp::send_event::models::{SendEventRequest, SendEventResponse};

/// ### /api/v3/botx/smartapps/event
/// 
/// Отправка smartapp события
/// 
/// ### Описание
/// * Обрабатывается асинхронно
#[tracing::instrument(level = "debug")]
pub async fn send_event(context: &Arc<RwLock<BotXApiContext>>, request: &SendEventRequest) -> BotXApiResult<SendEventResponse, serde_json::Value> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.send_event_api_builder)(&context_read_lock.cts_url);
    drop(context_read_lock);

    RequestManager::post(
        url,
        request,
        RequestManager::JSON_CONTENT_TYPE,
        context,
    ).await
}
