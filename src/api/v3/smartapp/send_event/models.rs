use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::api::models::AsyncFile;



/// Модель запроса отправки события в смартап
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct SendEventRequest {
    /// уникальный идентификатор запроса
    #[serde(rename(serialize = "ref", deserialize = "ref"))] // ref зарезервировано
    pub request_ref: Uuid,

    /// ID смарт аппа
    pub smartapp_id: Uuid,

    /// ID чата пользователя привязанного к смартапп
    pub group_chat_id: Uuid,

    /// пользовательские данные
    pub data: serde_json::Value,

    /// опции запроса
    pub opts: serde_json::Value,

    /// версия протокола smartapp <-> bot
    pub smartapp_api_version: u16,

    /// (Default: []) - массив файлов в base64
    #[builder(default)]
    pub files: Vec<File>,

    /// метаданные файлов для отложенной обработки
    #[builder(default)] 
    pub async_files: Vec<AsyncFile>,

    /// (Default: true) - шифровать или нет payload события. Не влияет на шифрование файлов.
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub encrypted: Option<bool>,
}

/// Модель файла
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct File {
    /// имя файла
    pub file_name: String,

    /// data URL + base64 data (RFC 2397)
    pub data: String,

    /// (Default: null) - текст под файлом
    pub caption: Option<String>,

    /// (Default: null) - ID файла с которым он будет загружен и сохранен в File Service
    pub file_id: Option<Uuid>,
}

/// Модель ответа на запрос отправки события в смартап
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SendEventResponse {
    /// Результат запроса
    /// 
    /// TODO: Я не знаю зачем тут строка, всегда должно быть "smartapp_notification_pushed" документация не описывает других вариантов, откройте issue если станет понятно что оно имеет несколько значений чтобы заменить на enum
    pub result: String,
}