use serde::{Serialize, Deserialize};

use super::{handler::Handler, align::Align};

/// кнопки команд расположенные на клавиатуре, представленные в виде двумерного массива объектов.
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct Button {
    /// тело команды (текст, который будет отправлен от пользователя и придёт в поле body)
    pub command: String,

    /// наименование команды (текст кнопки)
    pub label: String,

    /// (Default: {}) - объект с данными, которые будут отправлены в качестве параметров команды при нажатие на кнопку
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub data: Option<serde_json::Value>,

    /// объект с клиентскими опциями кнопки
    #[builder(default)]
    pub opts: ButtonOptions,
}

/// объект с клиентскими опциями кнопки
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct ButtonOptions {
    /// (Default: false) - если значение true, то при нажатие на кнопку в чат не будет отправлено сообщение с текстом команды и сама команда отправится боту в фоне
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub silent: Option<bool>,

    /// (Default: 1) - размер кнопки по горизонтали
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub h_size: Option<u32>,

    /// (Default: false) - если значение true, то при нажатии на кнопку отобразится всплывающее уведомление с заданным в alert_text сообщением
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub show_alert: Option<bool>,

    /// (Default: null) - текст уведомления. Если значение null, то выведется тело команды
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub alert_text: Option<String>,

    /// (Default: “bot”) - если значение “client”, то при нажатии на кнопку команда не должна отправляться боту, а должна обрабатываться самим клиентом
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub handler: Option<Handler>,
    
    /// (Default: null) - цвет текста в hex формате
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub font_color: Option<String>, // TODO: HEX

    /// (Default: null) - цвет фона/границ в hex формате
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub background_color: Option<String>, //TODO: HEX

    ///  (Default: “left”) - выравнивание текста left|center|right
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub align: Option<Align>,
}