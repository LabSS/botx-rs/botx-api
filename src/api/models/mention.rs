use serde::{Serialize, Deserialize};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, Default, PartialEq, Eq, Clone, Builder)]
#[builder(setter(into, prefix = "with"))]
/// список меншнов 
pub struct Mention {
    /// (Default: “user”) - тип меншна user|chat|channel|contact|all
    #[serde(skip_serializing_if = "Option::is_none")]
    pub mention_type: Option<MentionType>,

    /// id меншна (uuid5)
    pub mention_id: Uuid,

    /// цель mention. Для mention_type “all” - null
    #[serde(skip_serializing_if = "Option::is_none")]
    pub mention_data: Option<MentionData>
}

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone, Copy, Default)]
pub enum MentionType {
    #[default]
    #[serde(rename(serialize = "user", deserialize = "user"))]
    User,
    #[serde(rename(serialize = "chat", deserialize = "chat"))]
    Chat,
    #[serde(rename(serialize = "channel", deserialize = "channel"))]
    Channel,
    #[serde(rename(serialize = "contact", deserialize = "contact"))]
    Contact,
    #[serde(rename(serialize = "all", deserialize = "all"))]
    All,
}

/// цель mention
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone)]
#[serde(untagged)]
pub enum MentionData {
    /// для mention_type “user” и “contact”
    PersonMention(PersonMentionData),
    /// для mention_type “chat” и “channel”
    GroupMention(GroupMentionData)
}

/// для mention_type “user” и “contact”
#[derive(Debug, Serialize, Deserialize, Default, PartialEq, Eq, Clone, Builder)]
#[builder(setter(into, prefix = "with"))]
pub struct PersonMentionData {
    /// huid пользователя которого меншат (uuid5)
    pub user_huid: Uuid,

    /// имя пользователя
    pub name: String,

    /// тип соединения контакта
    pub conn_type: String,
}

/// для mention_type “chat” и “channel”
#[derive(Debug, Serialize, Deserialize, Default, PartialEq, Eq, Clone, Builder)]
#[builder(setter(into, prefix = "with"))]
pub struct GroupMentionData {
    /// id чата
    pub group_chat_id: Uuid,
    
    /// отображаемое имя чата (uuid5)
    pub name: String,
}