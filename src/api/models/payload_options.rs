use serde::{Serialize, Deserialize};

/// опции запроса
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct PayloadOptions {
    /// (Default: false) - если true, то сообщение будет отправлено в чат только в том случае, если в чате включен стелс режим
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub stealth_mode: Option<bool>,

    #[builder(default)]
    pub notification_opts: PayloadOptionsNotificationOptions,
}

#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct PayloadOptionsNotificationOptions {
    /// (Default: true) - отправлять/не отправлять пуш нотификацию
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub send: Option<bool>,

    /// (Default: false) - игнорировать/не игнорировать DND/Mute.
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub force_dnd: Option<bool>,
}