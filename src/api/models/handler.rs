use serde::{Serialize, Deserialize};

/// если значение “client”, то при нажатии на кнопку команда не должна отправляться боту, а должна обрабатываться самим клиентом
#[derive(Debug, Serialize, Deserialize, Default, Clone)]
pub enum Handler {
    #[default]
    #[serde(rename(serialize = "bot", deserialize = "bot"))]
    Bot,
    #[serde(rename(serialize = "client", deserialize = "client"))]
    Client,
}