use serde::{Serialize, Deserialize};


#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Default, Clone, Copy)]
pub enum MimeType {
    #[serde(rename(serialize = "application/epub+zip", deserialize = "application/epub+zip"))]
    Epub,
    #[serde(rename(serialize = "application/gzip", deserialize = "application/gzip"))]
    Gz,
    #[serde(rename(serialize = "application/java-archive", deserialize = "application/java-archive"))]
    Jar,
    #[serde(rename(serialize = "application/javascript", deserialize = "application/javascript"))]
    AppJs,
    #[serde(rename(serialize = "application/json", deserialize = "application/json"))]
    Json,
    #[serde(rename(serialize = "application/json-patch+json", deserialize = "application/json-patch+json"))]
    JsonPatch,
    #[serde(rename(serialize = "application/ld+json", deserialize = "application/ld+json"))]
    Jsonld,
    #[serde(rename(serialize = "application/manifest+json", deserialize = "application/manifest+json"))]
    Webmanifest,
    #[serde(rename(serialize = "application/msword", deserialize = "application/msword"))]
    Doc,
    #[serde(rename(serialize = "application/octet-stream", deserialize = "application/octet-stream"))]
    Bin,
    #[serde(rename(serialize = "application/ogg", deserialize = "application/ogg"))]
    Ogx,
    #[serde(rename(serialize = "application/pdf", deserialize = "application/pdf"))]
    Pdf,
    #[serde(rename(serialize = "application/postscript", deserialize = "application/postscript"))]
    Ps,
    #[serde(rename(serialize = "application/postscript", deserialize = "application/postscript_2"))]
    Eps,
    #[serde(rename(serialize = "application/postscript", deserialize = "application/postscript_3"))]
    Ai,
    #[serde(rename(serialize = "application/rtf", deserialize = "application/rtf"))]
    Rtf,
    #[serde(rename(serialize = "application/vnd.amazon.ebook", deserialize = "application/vnd.amazon.ebook"))]
    Azw,
    #[serde(rename(serialize = "application/vnd.api+json", deserialize = "application/vnd.api+json"))]
    JsonApi,
    #[serde(rename(serialize = "application/vnd.apple.installer+xml", deserialize = "application/vnd.apple.installer+xml"))]
    Mpkg,
    #[serde(rename(serialize = "application/vnd.mozilla.xul+xml", deserialize = "application/vnd.mozilla.xul+xml"))]
    Xul,
    #[serde(rename(serialize = "application/vnd.ms-excel", deserialize = "application/vnd.ms-excel"))]
    Xls,
    #[serde(rename(serialize = "application/vnd.ms-fontobject", deserialize = "application/vnd.ms-fontobject"))]
    Eot,
    #[serde(rename(serialize = "application/vnd.ms-powerpoint", deserialize = "application/vnd.ms-powerpoint"))]
    Ppt,
    #[serde(rename(serialize = "application/vnd.oasis.opendocument.presentation", deserialize = "application/vnd.oasis.opendocument.presentation"))]
    Odp,
    #[serde(rename(serialize = "application/vnd.oasis.opendocument.spreadsheet", deserialize = "application/vnd.oasis.opendocument.spreadsheet"))]
    Ods,
    #[serde(rename(serialize = "application/vnd.oasis.opendocument.text", deserialize = "application/vnd.oasis.opendocument.text"))]
    Odt,
    #[serde(rename(serialize = "application/vnd.openxmlformats-officedocument.presentationml.presentation", deserialize = "application/vnd.openxmlformats-officedocument.presentationml.presentation"))]
    Pptx,
    #[serde(rename(serialize = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", deserialize = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))]
    Xlsx,
    #[serde(rename(serialize = "application/vnd.openxmlformats-officedocument.wordprocessingml.document", deserialize = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"))]
    Docx,
    #[serde(rename(serialize = "application/vnd.rar", deserialize = "application/vnd.rar"))]
    Rar,
    #[serde(rename(serialize = "application/vnd.visio", deserialize = "application/vnd.visio"))]
    Vsd,
    #[serde(rename(serialize = "application/x-7z-compressed", deserialize = "application/x-7z-compressed"))]
    _7z,
    #[serde(rename(serialize = "application/x-abiword", deserialize = "application/x-abiword"))]
    Abw,
    #[serde(rename(serialize = "application/x-bzip", deserialize = "application/x-bzip"))]
    Bz,
    #[serde(rename(serialize = "application/x-bzip2", deserialize = "application/x-bzip2"))]
    Bz2,
    #[serde(rename(serialize = "application/x-cdf", deserialize = "application/x-cdf"))]
    Cda,
    #[serde(rename(serialize = "application/x-csh", deserialize = "application/x-csh"))]
    Csh,
    #[serde(rename(serialize = "application/x-freearc", deserialize = "application/x-freearc"))]
    Arc,
    #[serde(rename(serialize = "application/x-httpd-php", deserialize = "application/x-httpd-php"))]
    Php,
    #[serde(rename(serialize = "application/x-msaccess", deserialize = "application/x-msaccess"))]
    Mdb,
    #[serde(rename(serialize = "application/x-sh", deserialize = "application/x-sh"))]
    Sh,
    #[serde(rename(serialize = "application/x-shockwave-flash", deserialize = "application/x-shockwave-flash"))]
    Swf,
    #[serde(rename(serialize = "application/x-tar", deserialize = "application/x-tar"))]
    Tar,
    #[serde(rename(serialize = "application/xhtml+xml", deserialize = "application/xhtml+xml"))]
    Xhtml,
    #[serde(rename(serialize = "application/xml", deserialize = "application/xml"))]
    AppXml,
    #[serde(rename(serialize = "application/wasm", deserialize = "application/wasm"))]
    Wasm,
    #[serde(rename(serialize = "application/zip", deserialize = "application/zip"))]
    Zip,
    #[serde(rename(serialize = "audio/3gpp", deserialize = "audio/3gpp"))]
    Audio3gp,
    #[serde(rename(serialize = "audio/3gpp2", deserialize = "audio/3gpp2"))]
    Audio3g2,
    #[serde(rename(serialize = "audio/aac", deserialize = "audio/aac"))]
    Aac,
    #[serde(rename(serialize = "audio/midi", deserialize = "audio/midi"))]
    Mid,
    #[serde(rename(serialize = "audio/midi", deserialize = "audio/midi_2"))]
    Midi,
    #[serde(rename(serialize = "audio/mpeg", deserialize = "audio/mpeg"))]
    Mp3,
    #[serde(rename(serialize = "audio/ogg", deserialize = "audio/ogg"))]
    Oga,
    #[serde(rename(serialize = "audio/opus", deserialize = "audio/opus"))]
    Opus,
    #[serde(rename(serialize = "audio/wav", deserialize = "audio/wav"))]
    Wav,
    #[serde(rename(serialize = "audio/webm", deserialize = "audio/webm"))]
    Weba,
    #[serde(rename(serialize = "font/otf", deserialize = "font/otf"))]
    Otf,
    #[serde(rename(serialize = "font/ttf", deserialize = "font/ttf"))]
    Ttf,
    #[serde(rename(serialize = "font/woff", deserialize = "font/woff"))]
    Woff,
    #[serde(rename(serialize = "font/woff2", deserialize = "font/woff2"))]
    Woff2,
    #[serde(rename(serialize = "image/avif", deserialize = "image/avif"))]
    Avif,
    #[serde(rename(serialize = "image/bmp", deserialize = "image/bmp"))]
    Bmp,
    #[serde(rename(serialize = "image/gif", deserialize = "image/gif"))]
    Gif,
    #[serde(rename(serialize = "image/jpeg", deserialize = "image/jpeg"))]
    Jpg,
    #[serde(rename(serialize = "image/jpeg", deserialize = "image/jpeg_2"))]
    Jpeg,
    #[serde(rename(serialize = "image/png", deserialize = "image/png"))]
    Png,
    #[serde(rename(serialize = "image/svg+xml", deserialize = "image/svg+xml"))]
    Svg,
    #[serde(rename(serialize = "image/svg+xml", deserialize = "image/svg+xml_2"))]
    Svgz,
    #[serde(rename(serialize = "image/tiff", deserialize = "image/tiff"))]
    Tiff,
    #[serde(rename(serialize = "image/tiff", deserialize = "image/tiff_2"))]
    Tif,
    #[serde(rename(serialize = "image/vnd.microsoft.icon", deserialize = "image/vnd.microsoft.icon"))]
    Ico,
    #[serde(rename(serialize = "image/webp", deserialize = "image/webp"))]
    Webp,
    #[serde(rename(serialize = "text/calendar", deserialize = "text/calendar"))]
    Ics,
    #[serde(rename(serialize = "text/css", deserialize = "text/css"))]
    Css,
    #[serde(rename(serialize = "text/csv", deserialize = "text/csv"))]
    Csv,
    #[serde(rename(serialize = "text/html", deserialize = "text/html"))]
    Html,
    #[serde(rename(serialize = "text/html", deserialize = "text/html_2"))]
    Htm,
    #[serde(rename(serialize = "text/javascript", deserialize = "text/javascript"))]
    TextJs,
    #[serde(rename(serialize = "text/javascript", deserialize = "text/javascript_2"))]
    Mjs,
    #[serde(rename(serialize = "text/plain", deserialize = "text/plain"))]
    Txt,
    #[serde(rename(serialize = "text/plain", deserialize = "text/plain_2"))]
    Text,
    #[serde(rename(serialize = "text/xml", deserialize = "text/xml"))]
    TextXml,
    #[serde(rename(serialize = "video/3gpp", deserialize = "video/3gpp"))]
    Video3gp,
    #[serde(rename(serialize = "video/3gpp2", deserialize = "video/3gpp2"))]
    Video3g2,
    #[serde(rename(serialize = "video/quicktime", deserialize = "video/quicktime"))]
    Mov,
    #[serde(rename(serialize = "video/mp2t", deserialize = "video/mp2t"))]
    Ts,
    #[serde(rename(serialize = "video/mp4", deserialize = "video/mp4"))]
    Mp4,
    #[serde(rename(serialize = "video/mpeg", deserialize = "video/mpeg"))]
    Mpeg,
    #[serde(rename(serialize = "video/mpeg", deserialize = "video/mpeg_2"))]
    Mpg,
    #[serde(rename(serialize = "video/ogg", deserialize = "video/ogg"))]
    Ogv,
    #[serde(rename(serialize = "video/webm", deserialize = "video/webm"))]
    Webm,
    #[serde(rename(serialize = "video/x-msvideo", deserialize = "video/x-msvideo"))]
    Avi,
    #[serde(rename(serialize = "video/x-ms-wmv", deserialize = "video/x-ms-wmv"))]
    Wmv,
    #[serde(rename(serialize = "application/octet-stream", deserialize = "application/octet-stream"))]
    #[default]
    Unknown,
}