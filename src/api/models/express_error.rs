use serde::{Serialize, Deserialize};
use uuid::Uuid;

/// Чат не найден
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ChatNotFoundWithEventId {
    /// id сообщения 
    pub sync_id: Uuid,

    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: ChatNotFoundWithEventIdData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ChatNotFoundWithEventIdData {
    /// id не найденного чата 
    pub group_chat_id: Uuid,

    /// Описание ошибки
    pub error_description: String,
}

/// Чат не найден
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ChatNotFound {
    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: ChatNotFoundData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ChatNotFoundData {
    /// id не найденного чата 
    pub group_chat_id: Uuid,

    /// Описание ошибки
    pub error_description: String,
}

/// Ошибка от Messaging сервиса
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ErrorFromMessagingServiceWithEventId {
    /// id сообщения 
    pub sync_id: Uuid,

    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: ErrorFromMessagingServiceWithEventIdData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ErrorFromMessagingServiceWithEventIdData {
    /// id чата из события 
    pub group_chat_id: Uuid,

    /// Причина ошибки
    pub reason: String,

    /// Описание ошибки
    pub error_description: String,
}

/// Ошибка от Messaging сервиса
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ErrorFromMessagingService {
    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: ErrorFromMessagingServiceData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct ErrorFromMessagingServiceData {
    /// id чата из события 
    pub group_chat_id: Uuid,

    /// Причина ошибки
    pub reason: String,

    /// Описание ошибки
    pub error_description: String,
}

/// Бот отправитель не является участником чата
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct BotIsNotAChatMember {
    /// id сообщения 
    pub sync_id: Uuid,

    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: BotIsNotAChatMemberData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct BotIsNotAChatMemberData {
    /// id чата
    pub group_chat_id: Uuid,

    /// id бота отправившего событие
    pub bot_id: Uuid,

    /// Описание ошибки
    pub error_description: String,
}

/// Итоговый список получателей события пуст
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct EventRecipientsListIsEmpty {
    /// id сообщения 
    pub sync_id: Uuid,

    /// Список ошибок
    pub errors: Vec<String>,

    /// Дополнительная информация об ошибке
    pub error_data: EventRecipientsListIsEmptyData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct EventRecipientsListIsEmptyData {
    /// id чата из события
    pub group_chat_id: Uuid,

    /// id бота отправившего событие
    pub bot_id: Uuid,

    /// Список получателей
    pub recipients_param: Vec<Uuid>,

    /// Описание ошибки
    pub error_description: String,
}