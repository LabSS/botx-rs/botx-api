use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::api::models::MimeType;


/// файл в base64 представление. 
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with"))]
pub struct File {
    /// имя файла
    pub file_name: String,

    /// data URL + base64 data (RFC 2397)
    pub data: String,
}

/// файл в base64 представление. 
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with"))]
pub struct FileWithCaption {
    /// имя файла
    pub file_name: String,

    /// data URL + base64 data (RFC 2397)
    pub data: String,

    /// (Default: null) - текст под файлом
    pub caption: Option<String>,
}

#[derive(Debug, Serialize, Deserialize, Default, PartialEq, Eq, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct AsyncFile {
    /// тип файла
    #[serde(rename(serialize = "type", deserialize = "type"))] // лексема type зарезервирована TODO: сделать енам для допустимых значений
    pub async_file_type: String,

    /// ссылка на файл
    pub file: String,

    /// mimetype файла
    pub file_mime_type: MimeType,

    /// имя файла
    pub file_name: String,

    /// (Default: Null) - ссылка на превью
    #[builder(default)]
    pub file_preview: Option<String>,

    /// (Default: Null) - высота превью в px
    #[builder(default)]
    pub file_preview_height: Option<u32>,

    /// (Default: Null) - ширина превью в px
    pub file_preview_width: Option<u32>,

    /// размер файла в байтах
    pub file_size: u32,

    /// хэш файла
    pub file_hash: String,

    /// “stream”
    pub file_encryption_algo: String,

    /// размер чанков
    pub chunk_size: u32,

    /// ID файла
    pub file_id: Uuid,

    /// (Default: Null) - длительность видео/аудио
    #[builder(default)]
    pub duration: Option<u32>,

    /// (Default: Null) - подпись под файлом
    #[builder(default)]
    pub caption: Option<String>,
}

// {
//     "caption":null,
//     "chunk_size":2097152,
//     "duration":null,
//     "file":"/uploads/files/acdd27e8-87c6-0987-23d4-8da20b491d72/5029dc8fde3043d891816b20c791f6ed.txt?v=1683489628402",
//     "file_encryption_algo": "stream",
//     "file_hash":"IkmhWLxuQcbg31qgjMqweXOsmYn5TJ7kHkfQ/jX5t2s=",
//     "file_id":"4fd998e3-8b3c-5f1d-b1d8-d991bd98ca1a",
//     "file_mime_type":"text/plain",
//     "file_name":"uploaded_file.txt",
//     "file_preview":null,
//     "file_preview_height":null,
//     "file_preview_width":null,
//     "file_size":45,
//     "type":"document"
// }