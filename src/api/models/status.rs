use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone, Copy, Default)]
pub enum Status {
    #[default]
    #[serde(rename(serialize = "ok", deserialize = "ok"))]
    Ok,
    #[serde(rename(serialize = "error", deserialize = "error"))]
    Error,
}