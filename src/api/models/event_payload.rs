use serde::{Serialize, Deserialize};

use crate::api::models::*;

#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct EventPayload {
    /// Служит идентификатором успешности или провала выполнения команды.
    #[builder(default)]
    pub status: Status,

    /// текстовое сообщение. Отображается в чате, как текстовое сообщение.
    #[builder(default)]
    pub body: String,

    /// (Default: {}) - метаданные которые будут отправлены в параметрах команды при нажатие на любую кнопку.
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub metadata: Option<serde_json::Value>,

    /// (Default: {}) - опции сообщения 
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub opts: Option<EventPayloadOptions>,

    /// (Default: []) - кнопки команд расположенные на клавиатуре, представленные в виде двумерного массива объектов.
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub keyboard: Option<Vec<Vec<Button>>>,

    /// (Default: []) - кнопки команд расположенные под сообщением, представленные в виде двумерного массива объектов. 
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub bubble: Option<Vec<Vec<Bubble>>>,

    /// (Default: []) - список меншнов
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub mentions: Option<Vec<Mention>>,
}

/// опции сообщения
#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct EventPayloadOptions {
    /// (Default: false) - если значение true, то последующие сообщения пользователя не будут отображаться в чате, <br/>
    /// до тех пор пока не придет сообщения бота у которого это значение будет установлено в false. <br/>
    /// Разрешено только в личных чатах (1-1)
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub silent_response: Option<bool>,

    /// (Default: false) - если значение ‘true’, то кнопки, при отображение, будут автоматически переноситься на новый ряд, <br/>
    /// если не помещаются в заданном ряду
    #[serde(skip_serializing_if = "Option::is_none")]
    #[builder(default)]
    pub buttons_auto_adjust: Option<bool>,
}