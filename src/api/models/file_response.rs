use crate::api::models::MimeType;

#[derive(Debug, Clone)]
pub struct FileResponse {
    pub content_type: MimeType,
    pub data: Vec<u8>,
}