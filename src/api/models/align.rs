use serde::{Serialize, Deserialize};

/// выравнивание текста left|center|right
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq, Clone, Copy, Default)]
pub enum Align {
    #[default]
    #[serde(rename(serialize = "left", deserialize = "left"))]
    Left,
    #[serde(rename(serialize = "center", deserialize = "center"))]
    Center,
    #[serde(rename(serialize = "right", deserialize = "right"))]
    Right,
}