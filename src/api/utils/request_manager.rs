use std::{fmt::{
    Display,
    Debug
}, sync::Arc};

use reqwest::{StatusCode, IntoUrl, header::CONTENT_TYPE};
use serde::{Serialize, Deserialize};
use async_lock::RwLock;

use crate::api::{
    result::{
        BotXApiError,
        ExpressResult,
        BotXApiResult,
    },
    context::BotXApiContext, v2::token::api::token, models::FileResponse
};

#[derive(Debug)]
pub struct MultipartPartData {
    pub name: String,
    pub file_name: Option<String>,
    pub content: MultipartPartDataContent,
    pub mime_type: Option<String>,
}

#[derive(Debug)]
pub enum MultipartPartDataContent {
    Bytes(Vec<u8>),
    Text(String)
}

pub struct RequestManager {}

impl RequestManager {
    pub const JSON_CONTENT_TYPE: &'static str = "application/json";
    pub const MULTIPART_CONTENT_TYPE: &'static str = "multipart/form-data";

    #[tracing::instrument(level = "debug")]
    pub async fn post<TRequest, TResponse, TError>(url: impl IntoUrl + Debug, request: &TRequest, content_type: impl Into<String> + Display + Debug, context: &Arc<RwLock<BotXApiContext>>) -> BotXApiResult<TResponse, TError>
    where
        TRequest: Serialize + Debug,
        for<'a> TResponse: Deserialize<'a> + Debug,
        for<'a> TError: Deserialize<'a> + Debug,
    {
        let context_read_lock = context.read().await;

        let raw_response = if context_read_lock.auth_token.is_some() {
            let auth_token = context_read_lock.auth_token.as_ref().map(|x| x.clone()).unwrap();

            let raw_response = context_read_lock.client.post(url)
                .bearer_auth(auth_token)
                .header(reqwest::header::CONTENT_TYPE, content_type.into())
                .json(request)
                .send()
                .await
                .map_err::<BotXApiError<TError>, _>(|e| {
                    tracing::error!("Ошибка запроса: [{e:#?}]");
                    e.into()
                })?;
                
            drop(context_read_lock);

            raw_response
        } else {            
            tracing::info!("Не найден токен авторизации в контексте бота. Пере запрашиваем токен авторизации");

            drop(context_read_lock);

            let mut context_write_lock = context.write().await;

            let token_response = token(&mut *context_write_lock).await;

            let Ok(token_result) = token_response else {
                tracing::error!("Не удалось повторить запрос с пере авторизацией");
                return Err(BotXApiError::Unauthorized);
            };

            context_write_lock.auth_token = Some(token_result.token.clone());

            let context_read_lock = async_lock::RwLockWriteGuard::<'_, BotXApiContext>::downgrade(context_write_lock);

            let auth_token = token_result.token;

            let raw_response = context_read_lock.client.post(url)
                .bearer_auth(auth_token)
                .header(reqwest::header::CONTENT_TYPE, content_type.into())
                .json(request)
                .send()
                .await
                .map_err::<BotXApiError<TError>, _>(|e| {
                    tracing::error!("Ошибка запроса: [{e:#?}]");
                    e.into()
                })?;
                
            drop(context_read_lock);

            raw_response
        };
    
        let status_code = raw_response.status();
        let response_body = raw_response.text()
            .await
            .map_err::<BotXApiError<TError>, _>(|e| {
                tracing::error!("Ошибка чтения запроса: [{e:#?}]");
                e.into()
            })?;
    
        if status_code == StatusCode::UNAUTHORIZED {
            tracing::debug!("Запрос не авторизован: [{response_body}]");
            return Err(BotXApiError::Unauthorized);
        }
    
        let response = serde_json::from_str::<ExpressResult<TResponse, TError>>(&*response_body)
            .map_err(|e| {
                tracing::error!("Ошибка десериализации ответа: [{response_body}]");
                BotXApiError::SerdeError(e)
            })?;
    
        if !response.is_ok() {
            tracing::error!("Результат зaпроса: status:[{status_code}] raw body:[{response_body}] deserialized body:[{response:#?}]");
        } else {
            tracing::debug!("Результат запроса status:[{status_code}] raw body:[{response_body}] deserialized body:[{response:#?}]");
        }
    
        response.into()
    } 

    #[tracing::instrument(level = "debug")]
    pub async fn put<TRequest, TResponse, TError>(url: impl IntoUrl + Debug, request: &TRequest, content_type: impl Into<String> + Display + Debug, context: &Arc<RwLock<BotXApiContext>>) -> BotXApiResult<TResponse, TError>
    where
        TRequest: Serialize + Debug,
        for<'a> TResponse: Deserialize<'a> + Debug,
        for<'a> TError: Deserialize<'a> + Debug,
    {
        let context_read_lock = context.read().await;

        let raw_response = if context_read_lock.auth_token.is_some() {
            let auth_token = context_read_lock.auth_token.as_ref().map(|x| x.clone()).unwrap();

            let raw_response = context_read_lock.client.put(url)
                .bearer_auth(auth_token)
                .header(reqwest::header::CONTENT_TYPE, content_type.into())
                .json(request)
                .send()
                .await
                .map_err::<BotXApiError<TError>, _>(|e| {
                    tracing::error!("Ошибка запроса: [{e:#?}]");
                    e.into()
                })?;
                
            drop(context_read_lock);

            raw_response
        } else {            
            tracing::info!("Не найден токен авторизации в контексте бота. Пере запрашиваем токен авторизации");

            drop(context_read_lock);

            let mut context_write_lock = context.write().await;

            let token_response = token(&mut *context_write_lock).await;

            let Ok(token_result) = token_response else {
                tracing::error!("Не удалось повторить запрос с пере авторизацией");
                return Err(BotXApiError::Unauthorized);
            };

            context_write_lock.auth_token = Some(token_result.token.clone());

            let context_read_lock = async_lock::RwLockWriteGuard::<'_, BotXApiContext>::downgrade(context_write_lock);

            let auth_token = token_result.token;

            let raw_response = context_read_lock.client.put(url)
                .bearer_auth(auth_token)
                .header(reqwest::header::CONTENT_TYPE, content_type.into())
                .json(request)
                .send()
                .await
                .map_err::<BotXApiError<TError>, _>(|e| {
                    tracing::error!("Ошибка запроса: [{e:#?}]");
                    e.into()
                })?;
                
            drop(context_read_lock);

            raw_response
        };
    
        let status_code = raw_response.status();
        let response_body = raw_response.text()
            .await
            .map_err::<BotXApiError<TError>, _>(|e| {
                tracing::error!("Ошибка чтения ответа: [{e:#?}]");
                e.into()
            })?;
    
        if status_code == StatusCode::UNAUTHORIZED {
            tracing::debug!("Зарпос не авторизован: [{response_body}]");
            return Err(BotXApiError::Unauthorized);
        }
    
        let response = serde_json::from_str::<ExpressResult<TResponse, TError>>(&*response_body)
            .map_err(|e| {
                tracing::error!("Ошибка десериализации ответа: [{response_body}]");
                BotXApiError::SerdeError(e)
            })?;
    
        if !response.is_ok() {
            tracing::error!("Результат зaпроса:: [{status_code}] raw body:[{response_body}] deserialized body:[{response:#?}]");
        } else {
            tracing::debug!("Результат зaпроса:: [{status_code}] raw body:[{response_body}] deserialized body:[{response:#?}]");
        }
    
        response.into()
    } 

    #[tracing::instrument(level = "debug")]
    pub async fn get<TResponse, TError>(url: impl IntoUrl + Debug, content_type: impl Into<String> + Display + Debug, context: &Arc<RwLock<BotXApiContext>>) -> BotXApiResult<TResponse, TError>
    where
        for<'a> TResponse: Deserialize<'a> + Debug,
        for<'a> TError: Deserialize<'a> + Debug,
    {
        let context_read_lock = context.read().await;

        let raw_response = if context_read_lock.auth_token.is_some() {
            let auth_token = context_read_lock.auth_token.as_ref().map(|x| x.clone()).unwrap();

            let raw_response = context_read_lock.client.get(url)
                .bearer_auth(auth_token)
                .header(reqwest::header::CONTENT_TYPE, content_type.into())
                .send()
                .await
                .map_err::<BotXApiError<TError>, _>(|e| {
                    tracing::error!("Ошибка запроса: [{e:#?}]");
                    e.into()
                })?;
                
            drop(context_read_lock);

            raw_response
        } else {            
            tracing::info!("Не найден токен авторизации в контексте бота. Пере запрашиваем токен авторизации");

            drop(context_read_lock);

            let mut context_write_lock = context.write().await;

            let token_response = token(&mut *context_write_lock).await;

            let Ok(token_result) = token_response else {
                tracing::error!("Не удалось повторить запрос с пере авторизацией");
                return Err(BotXApiError::Unauthorized);
            };

            context_write_lock.auth_token = Some(token_result.token.clone());

            let context_read_lock = async_lock::RwLockWriteGuard::<'_, BotXApiContext>::downgrade(context_write_lock);

            let auth_token = token_result.token;

            let raw_response = context_read_lock.client.get(url)
                .bearer_auth(auth_token)
                .header(reqwest::header::CONTENT_TYPE, content_type.into())
                .send()
                .await
                .map_err::<BotXApiError<TError>, _>(|e| {
                    tracing::error!("Ошибка запроса: [{e:#?}]");
                    e.into()
                })?;
                
            drop(context_read_lock);

            raw_response
        };
    
        let status_code = raw_response.status();
        let response_body = raw_response.text()
            .await
            .map_err::<BotXApiError<TError>, _>(|e| {
                tracing::error!("Ошибка чтения ответа: [{e:#?}]");
                e.into()
            })?;
    
        if status_code == StatusCode::UNAUTHORIZED {
            tracing::debug!("Запрос не авторизован: [{response_body}]");
            return Err(BotXApiError::Unauthorized);
        }
    
        let response = serde_json::from_str::<ExpressResult<TResponse, TError>>(&*response_body)
            .map_err(|e| {
                tracing::error!("Ошибка десериализации ответа: [{response_body}]");
                BotXApiError::SerdeError(e)
            })?;
    
        if !response.is_ok() {
            tracing::error!("Результат зaпроса: status:[{status_code}] raw body:[{response_body}] deserialized body:[{response:#?}]");
        } else {
            tracing::debug!("Результат зaпроса: status:[{status_code}] raw body:[{response_body}] deserialized body:[{response:#?}]");
        }
    
        response.into()
    }

    #[tracing::instrument(level = "debug")]
    pub async fn get_file<TError>(url: impl IntoUrl + Debug, content_type: impl Into<String> + Display + Debug, context: &Arc<RwLock<BotXApiContext>>) -> BotXApiResult<FileResponse, TError>
    where
        for<'a> TError: Deserialize<'a> + Debug,
    {
        let context_read_lock = context.read().await;

        let raw_response = if context_read_lock.auth_token.is_some() {
            let auth_token = context_read_lock.auth_token.as_ref().map(|x| x.clone()).unwrap();

            let raw_response = context_read_lock.client.get(url)
                .bearer_auth(auth_token)
                .header(reqwest::header::CONTENT_TYPE, content_type.into())
                .send()
                .await
                .map_err::<BotXApiError<TError>, _>(|e| {
                    tracing::error!("Ошибка запроса: [{e:#?}]");
                    e.into()
                })?;
                
            drop(context_read_lock);

            raw_response
        } else {            
            tracing::info!("Не найден токен авторизации в контексте бота. Пере запрашиваем токен авторизации");

            drop(context_read_lock);

            let mut context_write_lock = context.write().await;

            let token_response = token(&mut *context_write_lock).await;

            let Ok(token_result) = token_response else {
                tracing::error!("Не удалось повторить запрос с пере авторизацией");
                return Err(BotXApiError::Unauthorized);
            };

            context_write_lock.auth_token = Some(token_result.token.clone());

            let context_read_lock = async_lock::RwLockWriteGuard::<'_, BotXApiContext>::downgrade(context_write_lock);

            let auth_token = token_result.token;

            let raw_response = context_read_lock.client.get(url)
                .bearer_auth(auth_token)
                .header(reqwest::header::CONTENT_TYPE, content_type.into())
                .send()
                .await
                .map_err::<BotXApiError<TError>, _>(|e| {
                    tracing::error!("Ошибка запроса: [{e:#?}]");
                    e.into()
                })?;
                
            drop(context_read_lock);

            raw_response
        };
    
        let status_code = raw_response.status();

        // Нельзя сделать проверку на 2xx статусы, т.к. есть ошибка со статусом 204 (файл удален)
        let result = if [200u16, 201u16].contains(&raw_response.status().as_u16()) {
            let content_type = raw_response.headers()
                .get(CONTENT_TYPE)
                .map(|x| x.to_str()
                    .unwrap()
                    .to_string())
                .unwrap_or("text/plain".to_string());
            
            let data = raw_response.bytes()
                .await
                .map_err::<BotXApiError<TError>, _>(|e| {
                    tracing::error!("Ошибка чтения ответа: [{e:#?}]");
                    e.into()
                })?
                .to_vec();

            ExpressResult::Ok(FileResponse { content_type: serde_json::from_str(&*format!("\"{content_type}\"")).unwrap(), data })
        } else {
            let response_body = raw_response.text()
                .await
                .map_err::<BotXApiError<TError>, _>(|e| {
                    tracing::error!("Ошибка чтения ответа: [{e:#?}]");
                    e.into()
                })?;

            if status_code == StatusCode::UNAUTHORIZED {
                tracing::debug!("Запрос не авторизован: [{response_body}]");
                return Err(BotXApiError::Unauthorized);
            }

            let response_error = serde_json::from_str::<TError>(&*response_body).map_err(|e| {
                tracing::error!("Ошибка десериализации ответа: [{response_body}]");
                    BotXApiError::SerdeError(e)
                })?;
    
            tracing::error!("Результат зaпроса: status:[{status_code}] raw body:[{response_body}] deserialized body:[{response_error:#?}]");
        
            ExpressResult::Err(response_error)
        };

        result.into()
    }

    #[tracing::instrument(level = "debug")]
    pub async fn get_csv<TResponse, TError>(url: impl IntoUrl + Debug, content_type: impl Into<String> + Display + Debug, context: &Arc<RwLock<BotXApiContext>>) -> BotXApiResult<Vec<TResponse>, TError>
    where
        for<'a> TResponse: Deserialize<'a> + Debug,
        for<'a> TError: Deserialize<'a> + Debug,
    {
        let context_read_lock = context.read().await;

        let raw_response = if context_read_lock.auth_token.is_some() {
            let auth_token = context_read_lock.auth_token.as_ref().map(|x| x.clone()).unwrap();

            let raw_response = context_read_lock.client.get(url)
                .bearer_auth(auth_token)
                .header(reqwest::header::CONTENT_TYPE, content_type.into())
                .send()
                .await
                .map_err::<BotXApiError<TError>, _>(|e| {
                    tracing::error!("Ошибка запроса: [{e:#?}]");
                    e.into()
                })?;
                
            drop(context_read_lock);

            raw_response
        } else {            
            tracing::info!("Не найден токен авторизации в контексте бота. Пере запрашиваем токен авторизации");

            drop(context_read_lock);

            let mut context_write_lock = context.write().await;

            let token_response = token(&mut *context_write_lock).await;

            let Ok(token_result) = token_response else {
                tracing::error!("Не удалось повторить запрос с пере авторизацией");
                return Err(BotXApiError::Unauthorized);
            };

            context_write_lock.auth_token = Some(token_result.token.clone());

            let context_read_lock = async_lock::RwLockWriteGuard::<'_, BotXApiContext>::downgrade(context_write_lock);

            let auth_token = token_result.token;

            let raw_response = context_read_lock.client.get(url)
                .bearer_auth(auth_token)
                .header(reqwest::header::CONTENT_TYPE, content_type.into())
                .send()
                .await
                .map_err::<BotXApiError<TError>, _>(|e| {
                    tracing::error!("Ошибка запроса: [{e:#?}]");
                    e.into()
                })?;
                
            drop(context_read_lock);

            raw_response
        };
    
        let status_code = raw_response.status();
        let response_body = raw_response.text()
            .await
            .map_err::<BotXApiError<TError>, _>(|e| {
                tracing::error!("Ошибка чтения ответа: [{e:#?}]");
                e.into()
            })?;
    
        if status_code == StatusCode::UNAUTHORIZED {
            tracing::debug!("Запрос не авторизован: [{response_body}]");
            return Err(BotXApiError::Unauthorized);
        }

        if status_code.is_success() {
            let mut reader = csv::Reader::from_reader(response_body.as_bytes());
        
            let mut rows = vec![];
            for record in reader.deserialize() {
                let record: Result::<TResponse, _> = record;
                match record {
                    Ok(row) => rows.push(row),
                    Err(err) => tracing::error!("Не валидный формат данных, пропускаем строку:[{err:?}]"),
                }
            }

            return ExpressResult::<Vec::<TResponse>, TError>::Ok(rows).into();
        }
    
        let response = serde_json::from_str::<ExpressResult<Vec<TResponse>, TError>>(&*response_body)
            .map_err(|e| {
                tracing::error!("Ошибка десериализации ответа: [{response_body}]");
                BotXApiError::SerdeError(e)
            })?;
    
        if !response.is_ok() {
            tracing::error!("Результат зaпроса: status:[{status_code}] raw body:[{response_body}] deserialized body:[{response:#?}]");
        } else {
            tracing::debug!("Результат зaпроса: status:[{status_code}] raw body:[{response_body}] deserialized body:[{response:#?}]");
        }
    
        response.into()
    }

    #[tracing::instrument(level = "debug")]
    pub async fn delete<TResponse, TError>(url: impl IntoUrl + Debug, content_type: impl Into<String> + Display + Debug, context: &Arc<RwLock<BotXApiContext>>) -> BotXApiResult<TResponse, TError>
    where
        for<'a> TResponse: Deserialize<'a> + Debug,
        for<'a> TError: Deserialize<'a> + Debug,
    {
        let context_read_lock = context.read().await;

        let raw_response = if context_read_lock.auth_token.is_some() {
            let auth_token = context_read_lock.auth_token.as_ref().map(|x| x.clone()).unwrap();

            let raw_response = context_read_lock.client.delete(url)
                .bearer_auth(auth_token)
                .header(reqwest::header::CONTENT_TYPE, content_type.into())
                .send()
                .await
                .map_err::<BotXApiError<TError>, _>(|e| {
                    tracing::error!("Ошибка запроса: [{e:#?}]");
                    e.into()
                })?;
                
            drop(context_read_lock);

            raw_response
        } else {            
            tracing::info!("Не найден токен авторизации в контексте бота. Пере запрашиваем токен авторизации");

            drop(context_read_lock);

            let mut context_write_lock = context.write().await;

            let token_response = token(&mut *context_write_lock).await;

            let Ok(token_result) = token_response else {
                tracing::error!("Не удалось повторить запрос с пере авторизацией");
                return Err(BotXApiError::Unauthorized);
            };

            context_write_lock.auth_token = Some(token_result.token.clone());

            let context_read_lock = async_lock::RwLockWriteGuard::<'_, BotXApiContext>::downgrade(context_write_lock);

            let auth_token = token_result.token;

            let raw_response = context_read_lock.client.delete(url)
                .bearer_auth(auth_token)
                .header(reqwest::header::CONTENT_TYPE, content_type.into())
                .send()
                .await
                .map_err::<BotXApiError<TError>, _>(|e| {
                    tracing::error!("Ошибка запроса: [{e:#?}]");
                    e.into()
                })?;
                
            drop(context_read_lock);

            raw_response
        };
    
        let status_code = raw_response.status();
        let response_body = raw_response.text()
            .await
            .map_err::<BotXApiError<TError>, _>(|e| {
                tracing::error!("Ошибка чтения ответа: [{e:#?}]");
                e.into()
            })?;
    
        if status_code == StatusCode::UNAUTHORIZED {
            tracing::debug!("Запрос не авторизован: [{response_body}]");
            return Err(BotXApiError::Unauthorized);
        }
    
        let response = serde_json::from_str::<ExpressResult<TResponse, TError>>(&*response_body)
            .map_err(|e| {
                tracing::error!("Ошибка десериализации ответа: [{response_body}]");
                BotXApiError::SerdeError(e)
            })?;
    
        if !response.is_ok() {
            tracing::error!("Результат зaпроса: status:[{status_code}] raw body:[{response_body}] deserialized body:[{response:#?}]");
        } else {
            tracing::debug!("Результат зaпроса: status:[{status_code}] raw body:[{response_body}] deserialized body:[{response:#?}]");
        }
    
        response.into()
    }

    #[tracing::instrument(level = "debug")]
    pub async fn post_multipart<TResponse, TError>(url: impl IntoUrl + Debug + Debug, data: Vec<MultipartPartData>, context: &Arc<RwLock<BotXApiContext>>) -> BotXApiResult<TResponse, TError>
    where
        for<'a> TResponse: Deserialize<'a> + Debug,
        for<'a> TError: Deserialize<'a> + Debug,
    {
        let mut form = reqwest::multipart::Form::new();

        for data in data.into_iter() {
            let part = match data.content {
                MultipartPartDataContent::Bytes(bytes) => reqwest::multipart::Part::bytes(bytes),
                MultipartPartDataContent::Text(text) => reqwest::multipart::Part::text(text),
            };
            
            let part = if let Some(file_name) = data.file_name {
                part.file_name(file_name)
            } else {
                part
            };

            let part = if let Some(content_type) = data.mime_type {
                part.mime_str(&*content_type).unwrap()
            } else {
                part
            };

            form = form.part(data.name, part);
        }

        tracing::debug!("{:#?}", form);

        let context_read_lock = context.read().await;

        let raw_response = if context_read_lock.auth_token.is_some() {
            let auth_token = context_read_lock.auth_token.as_ref().map(|x| x.clone()).unwrap();

            let raw_response = context_read_lock.client.post(url)
                .bearer_auth(auth_token)
                .multipart(form)
                .send()
                .await
                .map_err::<BotXApiError<TError>, _>(|e| {
                    tracing::error!("Ошибка запроса: [{e:#?}]");
                    e.into()
                })?;
                
            drop(context_read_lock);

            raw_response
        } else {            
            tracing::info!("Не найден токен авторизации в контексте бота. Пере запрашиваем токен авторизации");

            drop(context_read_lock);

            let mut context_write_lock = context.write().await;

            let token_response = token(&mut *context_write_lock).await;

            let Ok(token_result) = token_response else {
                tracing::error!("Не удалось повторить запрос с пере авторизацией");
                return Err(BotXApiError::Unauthorized);
            };

            context_write_lock.auth_token = Some(token_result.token.clone());

            let context_read_lock = async_lock::RwLockWriteGuard::<'_, BotXApiContext>::downgrade(context_write_lock);

            let auth_token = token_result.token;

            let raw_response = context_read_lock.client.post(url)
                .bearer_auth(auth_token)
                .multipart(form)
                .send()
                .await
                .map_err::<BotXApiError<TError>, _>(|e| {
                    tracing::error!("Ошибка запроса: [{e:#?}]");
                    e.into()
                })?;
                
            drop(context_read_lock);

            raw_response
        };
    
        let status_code = raw_response.status();
        let response_body = raw_response.text()
            .await
            .map_err::<BotXApiError<TError>, _>(|e| {
                tracing::error!("Ошибка чтения ответа: [{e:#?}]");
                e.into()
            })?;
    
        if status_code == StatusCode::UNAUTHORIZED {
            tracing::debug!("Запрос не авторизован: [{response_body}]");
            return Err(BotXApiError::Unauthorized);
        }
    
        let response = serde_json::from_str::<ExpressResult<TResponse, TError>>(&*response_body)
            .map_err(|e| {
                tracing::error!("Ошибка десериализации ответа: [{response_body}]");
                BotXApiError::SerdeError(e)
            })?;
    
        if !response.is_ok() {
            tracing::error!("Результат зaпроса: status:[{status_code}] raw body:[{response_body}] deserialized body:[{response:#?}]");
        } else {
            tracing::debug!("Результат зaпроса: status:[{status_code}] raw body:[{response_body}] deserialized body:[{response:#?}]");
        }
    
        response.into()
    } 
}