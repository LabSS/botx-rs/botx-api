use sha2::Sha256;
use hmac::{Hmac, Mac};
use uuid::Uuid;

type HmacSha256 = Hmac<Sha256>;

pub struct ApiEndpoints {
    #[cfg(feature = "token")]
    pub token_api_builder: Box<dyn Fn(&String, &String, &String) -> String + Sync + Send>,

    #[cfg(feature = "notifications-direct")]
    pub direct_notification_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,

    #[cfg(feature = "notifications-internal")]
    pub internal_notification_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,

    #[cfg(feature = "events-edit_event")]
    pub edit_event_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,

    #[cfg(feature = "events-reply_event")]
    pub reply_event_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,

    #[cfg(feature = "events-status")]
    pub event_status_api_builder: Box<dyn Fn(&String, &Uuid) -> String + Sync + Send>,

    #[cfg(feature = "events-typing")]
    pub typing_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,

    #[cfg(feature = "events-stop_typing")]
    pub stop_typing_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,

    #[cfg(feature = "files-upload")]
    pub upload_file_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,

    #[cfg(feature = "files-download")]
    pub download_file_api_builder: Box<dyn Fn(&String, &Uuid, &Uuid, &bool) -> String + Sync + Send>,

    #[cfg(feature = "stickers-new_sticker_pack")]
    pub new_sticker_pack_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,
    
    #[cfg(feature = "stickers-sticker_packs_list")]
    pub sticker_packs_list_api_builder: Box<dyn Fn(&String, &Option<Uuid>, &Option<u32>, &Option<String>) -> String + Sync + Send>,

    #[cfg(feature = "stickers-add_sticker")]
    pub add_sticker_api_builder: Box<dyn Fn(&String, &Uuid) -> String + Sync + Send>,

    #[cfg(feature = "stickers-get_sticker_pack")]
    pub get_sticker_pack_api_builder: Box<dyn Fn(&String, &Uuid) -> String + Sync + Send>,

    #[cfg(feature = "stickers-get_sticker")]
    pub get_sticker_api_builder: Box<dyn Fn(&String, &Uuid, &Uuid) -> String + Sync + Send>,

    #[cfg(feature = "stickers-update_sticker_pack")]
    pub update_sticker_pack_api_builder: Box<dyn Fn(&String, &Uuid) -> String + Sync + Send>,

    #[cfg(feature = "stickers-delete_sticker_pack")]
    pub delete_sticker_pack_api_builder: Box<dyn Fn(&String, &Uuid) -> String + Sync + Send>,

    #[cfg(feature = "stickers-delete_sticker")]
    pub delete_sticker_api_builder: Box<dyn Fn(&String, &Uuid, &Uuid) -> String + Sync + Send>,

    #[cfg(feature = "chats-list")]
    pub get_chats_list_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,

    #[cfg(feature = "chats-info")]
    pub get_chat_info_api_builder: Box<dyn Fn(&String, &Uuid) -> String + Sync + Send>,

    #[cfg(feature = "chats-add_user")]
    pub add_user_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,
    
    #[cfg(feature = "chats-remove_user")]
    pub remove_user_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,
    
    #[cfg(feature = "chats-add_admin")]
    pub add_admin_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,
    
    #[cfg(feature = "chats-set_stealth")]
    pub set_stealth_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,
    
    #[cfg(feature = "chats-disable_stealth")]
    pub disable_stealth_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,
    
    #[cfg(feature = "chats-create")]
    pub create_chat_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,
    
    #[cfg(feature = "chats-pin_message")]
    pub pin_message_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,
    
    #[cfg(feature = "chats-unpin_message")]
    pub unpin_message_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,
    
    #[cfg(feature = "users-by_email")]
    pub get_users_by_email_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,
    
    #[cfg(feature = "users-by_huid")]
    pub get_user_by_huid_api_builder: Box<dyn Fn(&String, &Uuid) -> String + Sync + Send>,
    
    #[cfg(feature = "users-by_login")]
    pub get_user_by_login_api_builder: Box<dyn Fn(&String, &String, &String) -> String + Sync + Send>,
    
    #[cfg(feature = "users-by_other_id")]
    pub get_user_by_other_id_api_builder: Box<dyn Fn(&String, &String) -> String + Sync + Send>,

    #[cfg(feature = "users-users_as_csv")]
    pub get_users_as_csv_api_builder: Box<dyn Fn(&String, bool, bool, bool) -> String + Sync + Send>,

    #[cfg(feature = "smartapp-event")]
    pub send_event_api_builder: Box<dyn Fn(&String) -> String + Sync + Send>,
}

impl std::fmt::Debug for ApiEndpoints {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ApiEndpoints").finish()
    }
}

impl Default for ApiEndpoints {
    fn default() -> Self { Self::new() }
}

impl ApiEndpoints {
    pub fn new() -> Self {
        Self {
            #[cfg(feature = "token")]
            token_api_builder: Self::get_token_api_builder(),
            #[cfg(feature = "notifications-direct")]
            direct_notification_api_builder: Self::get_direct_notification_api_builder(),
            #[cfg(feature = "notifications-internal")]
            internal_notification_api_builder: Self::get_internal_notification_api_builder(),
            #[cfg(feature = "events-edit_event")]
            edit_event_api_builder: Self::get_edit_event_api_builder(),
            #[cfg(feature = "events-reply_event")]
            reply_event_api_builder: Self::get_reply_event_api_builder(),
            #[cfg(feature = "events-status")]
            event_status_api_builder: Self::get_event_status_api_builder(),
            #[cfg(feature = "events-typing")]
            typing_api_builder: Self::get_typing_api_builder(),
            #[cfg(feature = "events-stop_typing")]
            stop_typing_api_builder: Self::get_stop_typing_api_builder(),
            #[cfg(feature = "files-upload")]
            upload_file_api_builder: Self::get_upload_file_api_builder(),
            #[cfg(feature = "files-download")]
            download_file_api_builder: Self::get_download_file_api_builder(),
            #[cfg(feature = "stickers-new_sticker_pack")]
            new_sticker_pack_api_builder: Self::get_new_sticker_pack_api_builder(),
            #[cfg(feature = "stickers-sticker_packs_list")]
            sticker_packs_list_api_builder: Self::get_sticker_packs_list_api_builder(),
            #[cfg(feature = "stickers-add_sticker")]
            add_sticker_api_builder: Self::get_add_sticker_api_builder(),
            #[cfg(feature = "stickers-get_sticker_pack")]
            get_sticker_pack_api_builder: Self::get_get_sticker_pack_api_builder(),
            #[cfg(feature = "stickers-get_sticker")]
            get_sticker_api_builder: Self::get_get_sticker_api_builder(),
            #[cfg(feature = "stickers-update_sticker_pack")]
            update_sticker_pack_api_builder: Self::get_update_sticker_pack_api_builder(),
            #[cfg(feature = "stickers-delete_sticker_pack")]
            delete_sticker_pack_api_builder: Self::get_delete_sticker_pack_api_builder(),
            #[cfg(feature = "stickers-delete_sticker")]
            delete_sticker_api_builder: Self::get_delete_sticker_api_builder(),
            #[cfg(feature = "chats-list")]
            get_chats_list_api_builder: Self::get_get_chats_list_api_builder(),
            #[cfg(feature = "chats-info")]
            get_chat_info_api_builder: Self::get_get_chat_info_api_builder(),
            #[cfg(feature = "chats-add_user")]
            add_user_api_builder: Self::get_add_user_api_builder(),
            #[cfg(feature = "chats-remove_user")]
            remove_user_api_builder: Self::get_remove_user_api_builder(),
            #[cfg(feature = "chats-add_admin")]
            add_admin_api_builder: Self::get_add_admin_api_builder(),
            #[cfg(feature = "chats-set_stealth")]
            set_stealth_api_builder: Self::get_set_stealth_api_builder(),
            #[cfg(feature = "chats-disable_stealth")]
            disable_stealth_api_builder: Self::get_disable_stealth_api_builder(),
            #[cfg(feature = "chats-create")]
            create_chat_api_builder: Self::get_create_chat_api_builder(),
            #[cfg(feature = "chats-pin_message")]
            pin_message_api_builder: Self::get_pin_message_api_builder(),
            #[cfg(feature = "chats-unpin_message")]
            unpin_message_api_builder: Self::get_unpin_message_api_builder(),
            #[cfg(feature = "users-by_email")]
            get_users_by_email_api_builder: Self::get_get_users_by_email_api_builder(),
            #[cfg(feature = "users-by_huid")]
            get_user_by_huid_api_builder: Self::get_get_user_by_huid_api_builder(),
            #[cfg(feature = "users-by_login")]
            get_user_by_login_api_builder: Self::get_get_user_by_login_api_builder(),
            #[cfg(feature = "users-by_other_id")]
            get_user_by_other_id_api_builder: Self::get_get_user_by_other_id_api_builder(),
            #[cfg(feature = "users-users_as_csv")]
            get_users_as_csv_api_builder: Self::get_get_users_as_csv_api_builder(),
            #[cfg(feature = "smartapp-event")]
            send_event_api_builder: Self::get_send_event_api_builder(),
        }
    }

    #[cfg(feature = "token")]
    fn get_token_api_builder() -> Box<dyn Fn(&String, &String, &String) -> String + Sync + Send> {
        Box::new(|cts_url: &String, bot_id: &String, secret_key: &String| {
            let mut mac = HmacSha256::new_from_slice(secret_key.as_bytes())
                .expect("invalid secret key");
    
            mac.update(bot_id.as_bytes());
        
            let result = mac.finalize();
        
            format!("{}/api/v2/botx/bots/{}/token?signature={:02X}", cts_url, bot_id, result.into_bytes())
        })
    }

    #[cfg(feature = "notifications-direct")]
    fn get_direct_notification_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v4/botx/notifications/direct")
        })
    }

    #[cfg(feature = "notifications-internal")]
    fn get_internal_notification_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v4/botx/notifications/internal")
        })
    }

    #[cfg(feature = "events-edit_event")]
    fn get_edit_event_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v3/botx/events/edit_event")
        })
    }

    #[cfg(feature = "events-reply_event")]
    fn get_reply_event_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v3/botx/events/reply_event")
        })
    }

    #[cfg(feature = "events-status")]
    fn get_event_status_api_builder() -> Box<dyn Fn(&String, &Uuid) -> String + Sync + Send> {
        Box::new(|cts_url, sync_id| {
            format!("{cts_url}/api/v3/botx/events/{sync_id}/status")
        })
    }

    #[cfg(feature = "events-typing")]
    fn get_typing_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v3/botx/events/typing")
        })
    }

    #[cfg(feature = "events-stop_typing")]
    fn get_stop_typing_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v3/botx/events/stop_typing")
        })
    }

    #[cfg(feature = "files-upload")]
    fn get_upload_file_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v3/botx/files/upload")
        })
    }

    #[cfg(feature = "files-download")]
    fn get_download_file_api_builder() -> Box<dyn Fn(&String, &Uuid, &Uuid, &bool) -> String + Sync + Send> {
        Box::new(|cts_url, group_chat_id, file_id, is_preview| {
            format!("{cts_url}/api/v3/botx/files/download?group_chat_id={group_chat_id}&file_id={file_id}&is_preview={is_preview}")
        })
    }

    #[cfg(feature = "stickers-new_sticker_pack")]
    fn get_new_sticker_pack_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v3/botx/stickers/packs")
        })
    }
    
    #[cfg(feature = "stickers-sticker_packs_list")]
    fn get_sticker_packs_list_api_builder() -> Box<dyn Fn(&String, &Option<Uuid>, &Option<u32>, &Option<String>) -> String + Sync + Send> {
        Box::new(|cts_url, user_huid, limit, after| {
            let has_any_param = user_huid.is_some() || limit.is_some() || after.is_some();
            
            let params = vec![
                user_huid.as_ref().map(|x| format!("user_huid={x}")),
                limit.as_ref().map(|x| format!("limit={x}")),
                after.as_ref().map(|x| format!("after={x}")),
                ]
                .into_iter()
                .filter_map(|x| x)
                .collect::<Vec<String>>()
            .join("&");
        
        format!("{}/api/v3/botx/stickers/packs{}{}",
                cts_url,
                if has_any_param { "?" } else { "" },
                params)
            })
    }
        
    #[cfg(feature = "stickers-add_sticker")]
    fn get_add_sticker_api_builder() -> Box<dyn Fn(&String, &Uuid) -> String + Sync + Send> {
        Box::new(|cts_url, pack_id| {
            format!("{cts_url}/api/v3/botx/stickers/packs/{pack_id}/stickers")
        })
    }
        
    #[cfg(feature = "stickers-get_sticker_pack")]
    fn get_get_sticker_pack_api_builder() -> Box<dyn Fn(&String, &Uuid) -> String + Sync + Send> {
        Box::new(|cts_url, pack_id| {
            format!("{cts_url}/api/v3/botx/stickers/packs/{pack_id}")
        })
    }
        
    #[cfg(feature = "stickers-get_sticker")]
    fn get_get_sticker_api_builder() -> Box<dyn Fn(&String, &Uuid, &Uuid) -> String + Sync + Send> {
        Box::new(|cts_url, pack_id, sticker_id| {
            format!("{cts_url}/api/v3/botx/stickers/packs/{pack_id}/stickers/{sticker_id}")
        })
    }
        
    #[cfg(feature = "stickers-update_sticker_pack")]
    fn get_update_sticker_pack_api_builder() -> Box<dyn Fn(&String, &Uuid) -> String + Sync + Send> {
        Box::new(|cts_url, pack_id| {
            format!("{cts_url}/api/v3/botx/stickers/packs/{pack_id}/")
        })
    }
        
    #[cfg(feature = "stickers-delete_sticker_pack")]
    fn get_delete_sticker_pack_api_builder() -> Box<dyn Fn(&String, &Uuid) -> String + Sync + Send> {
        Box::new(|cts_url, pack_id| {
            format!("{cts_url}/api/v3/botx/stickers/packs/{pack_id}")
        })
    }
        
    #[cfg(feature = "stickers-delete_sticker")]
    fn get_delete_sticker_api_builder() -> Box<dyn Fn(&String, &Uuid, &Uuid) -> String + Sync + Send> {
        Box::new(|cts_url, pack_id, sticker_id| {
            format!("{cts_url}/api/v3/botx/stickers/packs/{pack_id}/stickers/{sticker_id}")
        })
    }
        
    #[cfg(feature = "chats-list")]
    fn get_get_chats_list_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v3/botx/chats/list")
        })
    }
        
    #[cfg(feature = "chats-info")]
    fn get_get_chat_info_api_builder() -> Box<dyn Fn(&String, &Uuid) -> String + Sync + Send> {
        Box::new(|cts_url, group_chat_id| {
            format!("{cts_url}/api/v3/botx/chats/info?group_chat_id={group_chat_id}")
        })
    }
        
    #[cfg(feature = "chats-add_user")]
    fn get_add_user_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v3/botx/chats/add_user")
        })
    }
    
    #[cfg(feature = "chats-remove_user")]
    fn get_remove_user_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v3/botx/chats/remove_user")
        })
    }

    #[cfg(feature = "chats-add_admin")]
    fn get_add_admin_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v3/botx/chats/add_admin")
        })
    }

    #[cfg(feature = "chats-set_stealth")]
    fn get_set_stealth_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v3/botx/chats/stealth_set")
        })
    }

    #[cfg(feature = "chats-disable_stealth")]
    fn get_disable_stealth_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v3/botx/chats/stealth_disable")
        })
    }

    #[cfg(feature = "chats-create")]
    fn get_create_chat_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v3/botx/chats/create")
        })
    }

    #[cfg(feature = "chats-pin_message")]
    fn get_pin_message_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v3/botx/chats/pin_message")
        })
    }

    #[cfg(feature = "chats-unpin_message")]
    fn get_unpin_message_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v3/botx/chats/unpin_message")
        })
    }

    #[cfg(feature = "users-by_email")]
    fn get_get_users_by_email_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v3/botx/users/by_email")
        })
    }

    #[cfg(feature = "users-by_huid")]
    fn get_get_user_by_huid_api_builder() -> Box<dyn Fn(&String, &Uuid) -> String + Sync + Send> {
        Box::new(|cts_url, user_huid| {
            format!("{cts_url}/api/v3/botx/users/by_huid?user_huid={user_huid}")
        })
    }

    #[cfg(feature = "users-by_login")]
    fn get_get_user_by_login_api_builder() -> Box<dyn Fn(&String, &String, &String) -> String + Sync + Send> {
        Box::new(|cts_url, ad_login, ad_domain| {
            format!("{cts_url}/api/v3/botx/users/by_login?ad_login={ad_login}&ad_domain={ad_domain}")
        })
    }

    #[cfg(feature = "users-by_other_id")]
    fn get_get_user_by_other_id_api_builder() -> Box<dyn Fn(&String, &String) -> String + Sync + Send> {
        Box::new(|cts_url, other_id| {
            format!("{cts_url}/api/v3/botx/users/by_other_id?other_id={other_id}")
        })
    }

    #[cfg(feature = "users-users_as_csv")]
    fn get_get_users_as_csv_api_builder() -> Box<dyn Fn(&String, bool, bool, bool) -> String + Sync + Send> {
        Box::new(|cts_url, cts_user, unregistered, botx| {
            format!("{cts_url}/api/v3/botx/users/users_as_csv?cts_user={cts_user}&unregistered={unregistered}&botx={botx}")
        })
    }

    #[cfg(feature = "smartapp-event")]
    fn get_send_event_api_builder() -> Box<dyn Fn(&String) -> String + Sync + Send> {
        Box::new(|cts_url| {
            format!("{cts_url}/api/v3/botx/smartapps/event")
        })
    }
}