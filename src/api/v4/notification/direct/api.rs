use std::sync::Arc;

use async_lock::RwLock;

use crate::api::{
    v4::notification::direct::models::{
        DirectNotificationResponse,
        DirectNotificationExpressError,
        DirectNotificationRequest,
    },
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager
};

/// ### /api/v4/botx/notifications/direct
/// 
/// Отправка директ нотификации v4
/// 
/// ### Описание
/// * Обрабатывается асинхронно
/// * В ответ на запрос приходит sync_id [UUID] - идентификатор отправляемого сообщения
/// * В случае успеха или провала отправки, на коллбек боту отправляется запрос с результатом отправки
#[tracing::instrument(level = "debug")]
pub async fn direct_notification(context: &Arc<RwLock<BotXApiContext>>, request: &DirectNotificationRequest) -> BotXApiResult<DirectNotificationResponse, DirectNotificationExpressError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.direct_notification_api_builder)(&context_read_lock.cts_url);
    drop(context_read_lock);

    RequestManager::post(
        url,
        request,
        RequestManager::MULTIPART_CONTENT_TYPE,
        context,
    ).await

    // log::info!("Direct notification v4 request url [{url}]");
    // log::debug!("Direct notification v4 request data [{}]", serde_json::to_string(request).unwrap());

    // let raw_response = context.client.post(url)
    //     .bearer_auth(token)
    //     .header(reqwest::header::CONTENT_TYPE, "multipart/form-data")
    //     .json(request)
    //     .send()
    //     .await
    //     .map_err::<BotXApiError<DirectNotificationExpressError>, _>(|e| {
    //         log::error!("Direct notification v4 request error:[{e:#?}]");
    //         e.into()
    //     })?;

    // let status_code = raw_response.status();
    // let response_body = raw_response.text()
    //     .await
    //     .map_err::<BotXApiError<DirectNotificationExpressError>, _>(|e| {
    //         log::error!("Direct notification v4 read response error:[{e:#?}]");
    //         e.into()
    //     })?;

    // if status_code == StatusCode::UNAUTHORIZED {
    //     log::debug!("Direct notification v4 request Unauthorized {response_body}");
    //     return Err(BotXApiError::Unauthorized);
    // }

    // let response = serde_json::from_str::<ExpressResult<DirectNotificationResponse, DirectNotificationExpressError>>(&*response_body)
    //     .map_err(|e| {
    //         log::error!("Direct notification v4 response body deserialization error: [{response_body}]");
    //         BotXApiError::SerdeError(e)
    //     })?;

    // if !response.is_ok() {
    //     log::error!("Direct notification v4 response status:[{status_code}] raw body:[{response_body}] deserialized body:[{response:#?}]");
    // } else {
    //     log::debug!("Direct notification v4 response status:[{status_code}] raw body:[{response_body}] deserialized body:[{response:#?}]");
    // }

    // response.into()
}