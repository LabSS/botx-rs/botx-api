#[cfg(feature = "notifications-direct")]
pub mod direct;
#[cfg(feature = "notifications-internal")]
pub mod internal;