use serde::{Serialize, Deserialize};
use uuid::Uuid;

use crate::api::{models::*};

#[derive(Debug, Serialize, Deserialize, Default, Clone, Builder)]
#[builder(setter(into, prefix = "with", strip_option))]
pub struct InternalNotificationRequest<TData, TOptions> {
    /// ID чата
    pub group_chat_id: Uuid,

    /// Пользовательские данные
    pub data: TData,

    /// (default: {}) - пользовательские опции
    pub opts: TOptions,
    
    /// (default: Null) - huid получателей события. По умолчанию все участники чата
    #[builder(default)]
    pub recipients: Option<Vec<Uuid>>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct InternalNotificationResponse {
    pub result: InternalNotificationResult,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct InternalNotificationResult {
    /// Идентификатор отправляемого сообщения
    pub sync_id: Uuid,
}

// #[derive(Debug, Serialize, Deserialize, Clone)]
// pub struct InternalNotificationExpressError {
//     pub sync_id: Uuid,
//     pub reason: String,
//     pub errors: Vec<String>,
//     pub error_data: InternalNotificationExpressErrorData
// }

#[derive(Debug, Serialize, Deserialize, Clone)]
#[serde(tag = "reason")]
/// Ошибки при отправке внутренней нотификации
pub enum InternalNotificationExpressError {
    /// Превышен лимит интенсивности запросов (429)
    #[serde(rename(serialize = "too_many_requests", deserialize = "too_many_requests"))]
    TooManyRequests(TooManyRequests),

    /// Чат не найден
    #[serde(rename(serialize = "chat_not_found", deserialize = "chat_not_found"))]
    ChatNotFound(ChatNotFoundWithEventId),

    /// Ошибка от Messaging сервиса
    #[serde(rename(serialize = "error_from_messaging_service", deserialize = "error_from_messaging_service"))]
    ErrorFromMessagingService(ErrorFromMessagingServiceWithEventId),

    /// Бот отправитель не является участником чата
    #[serde(rename(serialize = "bot_is_not_a_chat_member", deserialize = "bot_is_not_a_chat_member"))]
    BotIsNotAChatMember(BotIsNotAChatMember),

    /// Итоговый список получателей события пуст
    #[serde(rename(serialize = "event_recipients_list_is_empty", deserialize = "event_recipients_list_is_empty"))]
    EventRecipientsListIsEmpty(EventRecipientsListIsEmpty),

    // TODO: добавить десериализацию в HashMap<string, string> когда завезут реализацию
    /// Неопределенная ошибка, смотрите логи
    #[serde(other)]
    Other,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
/// Превышен лимит интенсивности запросов (429)
pub struct TooManyRequests {
    pub errors: Vec<String>,
    pub error_data: TooManyRequestsData,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
/// Чат не найден
pub struct TooManyRequestsData {
    pub bot_id: Uuid,
}

// pub struct ChatNotFound {

// }

// #[derive(Debug, Serialize, Deserialize, Clone)]
// pub struct InternalNotificationExpressErrorData {
//     bot_id: Option<Uuid>,

//     group_chat_id: Option<Uuid>,

//     /// Ожидается что будет всегда, но на всякий случай сделан опциональным
//     error_description: Option<String>,

//     reason: Option<String>,

//     recipients_param: Option<Vec<Uuid>>,
    
//     #[serde(flatten)]
//     other: HashMap<String, String>,
// }

