use std::{sync::Arc, fmt::Debug};

use serde::Serialize;
use async_lock::RwLock;

use crate::api::{
    v4::notification::internal::models::{
        InternalNotificationResponse,
        InternalNotificationExpressError,
        InternalNotificationRequest,
    },
    context::BotXApiContext,
    result::BotXApiResult,
    utils::request_manager::RequestManager
};

/// ### /api/v4/botx/notifications/internal
/// 
/// Отправка внутренней бот нотификации (ботам чата)
/// 
/// ### Описание
/// * Обрабатывается асинхронно
/// * В ответ на запрос приходит sync_id [UUID] - идентификатор отправляемого сообщения
/// * В случае успеха или провала отправки, на коллбек боту отправляется запрос с результатом отправки
#[tracing::instrument(level = "debug")]
pub async fn internal_notification<TData: Serialize + Debug, TOptions: Serialize + Debug>(context: &Arc<RwLock<BotXApiContext>>, request: &InternalNotificationRequest<TData, TOptions>) -> BotXApiResult<InternalNotificationResponse, InternalNotificationExpressError> {
    let context_read_lock = context.read().await;
    let url = (context_read_lock.api.internal_notification_api_builder)(&context_read_lock.cts_url);
    drop(context_read_lock);

    RequestManager::post(
        url,
        request,
        RequestManager::MULTIPART_CONTENT_TYPE,
        context,
    ).await
}