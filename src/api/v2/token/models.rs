use std::collections::HashMap;

use serde::{Serialize, Deserialize};


#[derive(Debug, Serialize, Deserialize)]
pub struct TokenResponse {
    //pub status: Status,
    #[serde(rename(serialize = "result", deserialize = "result"))]
    pub token: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct TokenExpressError {
    pub errors: Vec<String>,
    pub reason: String,
    pub error_data: HashMap<String, String>
}