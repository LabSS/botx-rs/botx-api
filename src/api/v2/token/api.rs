use reqwest::StatusCode;

use crate::api::{
    v2::token::models::*,
    context::BotXApiContext,
    result::*
};

/// ### /api/v2/botx/bots/:bot_id/token
/// Запрос токена авторизации (Bearer)
// Требуем write lock чтобы параллельно не шли запросы с инвалидной авторизацией
#[tracing::instrument(level = "debug")]
pub async fn token(context: &mut BotXApiContext) -> BotXApiResult<TokenResponse, TokenExpressError> {
    let url = (context.api.token_api_builder)(&context.cts_url, &context.bot_id, &context.secret_key);

    tracing::info!("Token v2 request url:[{url}]");

    let raw_response = context.client.get(url)
        .send()
        .await
        .map_err::<BotXApiError<TokenExpressError>, _>(|e| {
            tracing::error!("Token v2 request error:[{e:#?}]");
            e.into()
        })?;

    let status_code = raw_response.status();
    let response_body = raw_response.text()
        .await
        .map_err::<BotXApiError<TokenExpressError>, _>(|e| {
            tracing::error!("Token v2 read response error:[{e:#?}]");
            e.into()
        })?;

    if status_code == StatusCode::UNAUTHORIZED {
        tracing::debug!("Token v2 request Unauthorized: {response_body}");
        return Err(BotXApiError::Unauthorized);
    }

    let response = serde_json::from_str::<ExpressResult<TokenResponse, TokenExpressError>>(&*response_body)
        .map_err::<BotXApiError<TokenExpressError>, _>(|e| {
            tracing::error!("Token v2 response body deserialization error, error:[{e:#?}] response:[{response_body}]");
            e.into()
        })?;

    if !response.is_ok() {
        tracing::error!("Token v2 response status:[{status_code}] raw body:[{response_body}] deserialized body:[{response:#?}]");
    } else {
        tracing::debug!("Token v2 response status:[{status_code}] raw body:[{response_body}] deserialized body:[{response:#?}]");
    }

    response.into()
}