# botx-api

## Описание
Обертка для api eXpress

## Использование
Добавьте в Cargo.toml
```toml
botx-api = { version = "*", features = ["anthill-di"] }
```

Доступны следующие features:

`botx-api-v-all` - включение фич всех версий (default)    
`botx-api-v2` - включает все апи 2ой версии    
`botx-api-v3` - включает все апи 3ой версии    
`botx-api-v4` - включает все апи 4ой версии    
`anthill-di` - добавляет зависимость от anthill-di и async-trait-with-sync (добавляет конструктор регистрации BotXApiContext в anthill-di)    

`token` - включить апи аутентификации бота    
`notifications-internal` - включить апи отправки события (боту)    
`notifications-direct` - включить апи отправки события (сообщения)    
`events-edit_event` - включить апи редактирования события (сообщения)    
`events-reply_event` - включить апи ответа на событие (сообщение)    
`events-status` - включить апи запроса состояния события    
`events-typing` - включить апи включения состояния печати    
`events-stop_typing` - включить апи остановки состояния печати    
`files-upload` - включить апи загрузки фала    
`files-download` - включить апи скачивания фала    
`stickers-new_sticker_pack` - включить апи создания набора стикеров    
`stickers-sticker_packs_list` - включить апи запроса списка наборов стикеров    
`stickers-add_sticker` - включить апи добавления набора стикеров    
`stickers-get_sticker_pack` - включить апи запроса информации о наборе стикеров    
`stickers-get_sticker` - включить апи запроса информации о стикере    
`stickers-update_sticker_pack` - включить апи обновления набора стикеров    
`stickers-delete_sticker` - включить апи удаления стикера    
`stickers-delete_sticker_pack` - включить апи удаления набора стикеров    
`chats-list` - включить апи запроса списка чатов бота    
`chats-info` - включить апи запроса информации о чате бота    
`chats-add_use` - включить апи добавления пользователей в чат бота    
`chats-remove_use` - включить апи удаления пользователя из чата бота    
`chats-add_admin` - включить апи добавления администраторов в чата бота    
`chats-set_stealth` - включить апи включения стелс режима    
`chats-disable_stealth` - включить апи отключения стелс режима    
`chats-create` - включить апи создания нового чата    
`chats-pin_message` - включить апи закрепления сообщения в чате    
`chats-unpin_message` - включить апи открепления сообщения в чате    
`users-by_email` - включить апи запроса информации о пользователях по email адресам    
`users-by_huid` - включить апи запроса информации о пользователе по huid     
`chats-by_login` - включить апи запроса информации о пользователе по ad login + ad domain    
`chats-by_other_id` - включить апи запроса информации о пользователе по дополнительному id    
`users-users_as_csv` - включить апи запроса списка пользователей    

## Использование
Для использования создайте объект BotXApiContext
* Через метод new - с настройками клиента по умолчанию и конфигурацией бота
* Через метод from_client - с настроенным reqwest::async_impl::client::Client и конфигурацией бота
* Через метод from_env_with_default_client - с настройками клиента по умолчанию и конфигурацией бота из переменных окружения
* Через метод from_env - с настроенным reqwest::async_impl::client::Client и конфигурацией бота из переменных окружения
* Через регистрацию в anthill_di - зарегистрируйте BotXApiContext в контексте di методом extensions::IBotXApiContextExt::register_botx_api_context, опционально зарегистрируйте объекты BotXApiContextConfiguration как Singleton, reqwest::async_impl::client::Client, ApiEndpoints иначе будут взяты дефолтные настройки (конфигурация бота из переменных окружения)

Настройки бота берутся из переменных окружения:
* BOTX_API_CTS_URL - url cts сервера
* BOTX_API_BOT_ID - id бота
* BOTX_API_BOT_SECRET_KEY - секретный ключ бота

Все модели реализуют паттерн builder. Пример использования:
```rs
let internal_notification_request = InternalNotificationRequestBuilder::default()
    .with_group_chat_id(uuid::Uuid::from_str("de981642-5224-4e70-a8dd-9a933a8e096e"))
    .with_data(PingBotsCommandData { })
    .with_opts(PingBotsCommandOption { })
    .build()
    .expect("Не все поля запроса были указаны");
```

Для автоматического обновления авторизации добавлен метод retry_with_auth:
```rs
let internal_notification_request = InternalNotificationRequestBuilder::default()
    .with_group_chat_id(uuid::Uuid::from_str("de981642-5224-4e70-a8dd-9a933a8e096e"))
    .with_data(PingBotsCommandData { })
    .with_opts(PingBotsCommandOption { })
    .build()
    .expect("Не все поля запроса были указаны");

let internal_notification_result = retry_with_auth(&self.api, || internal_notification(&self.api, &internal_notification_request)).await;
```

При регистрации через anthill-di можно зарегистрировать свой объект ApiEndpoints, что позволяет менять api (бесполезно, но возможно)

Логика пишет логи через crate log. Включите логирование чтобы лучше понять работу вашего приложения

## Вклад
Откройте issue с вопросом или предложением

## Лицензия
MIT, можно использовать в любых целях

## Статус проекта
|Состояние|Метод|АPI|Описание|
|---|---|---|---|
|✅|GET|/api/v2/botx/bots/:bot_id/token||
|❌|POST|/api/v3/botx/notification/callback/direct|Отправка директ нотификации в чат (Deprecated)|
|✅|POST|/api/v4/botx/notifications/internal|Отправка внутренней бот нотификации|
|✅|POST|/api/v4/botx/notifications/direct|Отправка директ нотификации в чат|
|✅|POST|/api/v3/botx/events/edit_event|Редактирование события|
|✅|POST|/api/v3/botx/events/reply_event|Ответ (reply) на сообщение|
|✅|GET|/api/v3/botx/events/:sync_id/status|Статус сообщения|
|✅|POST|/api/v3/botx/events/typing|Отправка typing|
|✅|POST|/api/v3/botx/events/stop_typing|Отправка stop_typing|
|✅|GET|/api/v3/botx/chats/list|Получение списка чатов бота|
|✅|GET|/api/v3/botx/chats/info|Получение информации о чате|
|✅|POST|/api/v3/botx/chats/add_user|Добавление юзеров в чат|
|✅|POST|/api/v3/botx/chats/remove_user|Удаление юзеров из чата|
|✅|POST|/api/v3/botx/chats/add_admin|Добавление администратора в чат|
|✅|POST|/api/v3/botx/chats/stealth_set|Включение стелс режима в чате|
|✅|POST|/api/v3/botx/chats/stealth_disable|Отключение стелс режима в чате|
|✅|POST|/api/v3/botx/chats/create|Создание чата|
|✅|POST|/api/v3/botx/chats/pin_message|Закрепление сообщения в чате|
|✅|POST|/api/v3/botx/chats/unpin_message|Открепление сообщения в чате|
|❌|GET|/api/v3/botx/users/by_email|Поиск данных юзера по его почте (Deprecated)|
|✅|POST|/api/v3/botx/users/by_email|Поиск данных юзеров по их почтам|
|✅|GET|/api/v3/botx/users/by_huid|Поиск данных юзера по его huid|
|✅|GET|/api/v3/botx/users/by_login|Поиск данных юзера по его ad логину и ad домену|
|✅|GET|/api/v3/botx/users/by_other_id|Поиск данных юзера по дополнительному идентификатору|
|✅|GET|/api/v3/botx/users/users_as_csv|Получение списка пользователей на CTS|
|❌|GET|/api/v3/botx/users/update_profile|Обновление профиля юзера|
|❌|GET|/api/v3/botx/smartapps/list|Получение списка smartapp на CTS|
|❌|POST|/api/v3/botx/smartapps/event|Отправка smartapp события|
|❌|POST|/api/v3/botx/smartapps/notification|Отправка smartapp нотификации|
|❌|POST|/api/v3/botx/smartapps/upload_file|Загрузка smartapp файла|
|✅|GET|/api/v3/botx/stickers/packs|Получение списка наборов стикеров|
|✅|GET|/api/v3/botx/stickers/packs/:pack_id|Получение набора стикеров|
|✅|GET|/api/v3/botx/stickers/packs/:pack_id/stickers/:sticker_id|Получение стикера из набора стикеров|
|✅|POST|/api/v3/botx/stickers/packs|Создание набора стикеров|
|✅|POST|/api/v3/botx/stickers/packs/:pack_id/stickers|Добавление стикера в набор стикеров|
|✅|PUT|/api/v3/botx/stickers/packs/:pack_id|Редактирование набора стикеров|
|✅|DELETE|/api/v3/botx/stickers/packs/:pack_id|Удаление набора стикеров|
|✅|DELETE|/api/v3/botx/stickers/packs/:pack_id/stickers/:sticker_id|Удаление стикера из набора стикеров|
|✅|GET|/api/v3/botx/files/download|Скачивание файла|
|✅|POST|/api/v3/botx/files/upload|Загрузка файла|